:<<\_x
. $PROOT/$PV/guest/bin/podman.fun

. podman.fun && _podman_install
_docker_pull ubuntu
docker images
_x

#echo hglwoowfs
#DEBUG=2

SYSTEM_CONFIG_FILE=/etc/containers/registries.conf  # all users
:<<\_s
sudo mkdir -p /etc/containers/registries.conf.d
SYSTEM_CONFIG_FILE=/etc/containers/registries.conf.d/picasso.conf
_s

USER_CONFIG_FILE=~/.config/containers/registries.conf  # this user


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
function _podman_install() {

_debug "_podman_install @: $@"

which podman 1>/dev/null && return 0  # already installed?


# ----------
source /etc/os-release  # -> $VERSION_ID=xx.xx

sources_url="https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/xUbuntu_${VERSION_ID}"
key_url="https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/xUbuntu_${VERSION_ID}/Release.key"

echo "deb $sources_url/ /" | sudo tee /etc/apt/sources.list.d/devel:kubic:libcontainers:unstable.list
curl -fsSL $key_url | sudo gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/devel_kubic_libcontainers_unstable.gpg > /dev/null

sudo DEBIAN_FRONTEND=noninteractive apt-get -qq update && sudo DEBIAN_FRONTEND=noninteractive apt-get -qq install podman


# ----------
mkdir -p $HOME/.config/containers

:<<\_s
[[ -z "$DOCKERHUB_ENDPOINT" ]] && {

cat > $USER_CONFIG_FILE <<!
unqualified-search-registries = ['docker.io']

[[registry]]
location="docker.io"
!
}
_s


# ----------
:<<\_x
podman version
podman info
podman info --debug
podman search ubuntu
podman pull ubuntu
podman images
podman rmi ubuntu
podman search dockerhub.runnable.org:5001/hello-world
podman pull dockerhub.runnable.org:5001/library/hello-world
podman --log-level debug pull dockerhub.runnable.org:5001/hello-world
  No credentials for dockerhub.runnable.org:5001 found
curl https://dockerhub.runnable.org/index.html

podman pull --tls-verify=false library/ubuntu
podman pull --tls-verify=false ${DOCKERHUB_ENDPOINT#*://}/library/ubuntu:latest
podman pull --tls-verify=true docker.io/library/ubuntu:latest

docker --registry-mirror=$DOCKERHUB_ENDPOINT pull ubuntu
_x

:<<\_j
manifest unknown
typical of a missing image or one with incomplete/wrong metadata in the registry
_j

:<<\_s
#sudo usermod -aG docker $USER
sudo touch /etc/{subgid,subuid}
sudo usermod --add-subuids 100000-165535 --add-subgids 100000-165535 $USER
:<<\_x
grep $USER /etc/subuid /etc/subgid
  /etc/subuid:$USER:100000:65536
  /etc/subgid:$USER:100000:65536
_x
_s

# for this session...
shopt -s expand_aliases
alias docker=$(which podman)

# for other sessions...
cat >> ~/.bashrc <<!
shopt -s expand_aliases
alias docker=$(which podman)
!

:<<\_s
https://medium.com/devops-dudes/how-to-setup-root-less-podman-containers-efd109fa4e0d

# Note: As we know podman is daemonless, so this container is stopped on next boot.
# To make it persistent...

# Enable linger
$ sudo loginctl enable-linger {USER}
$ sudo loginctl user-status {USER}

# Create user systemd service for a container
$ cd .config
$ mkdir -p systemd/user
$ cd systemd/user
$ podman generate systemd --name linkding --files
$ systemctl --user daemon-reload
$ systemctl --user enable --now container-linkding.service
$ systemctl --user status container-linkding.service
_s

:<<\_c
https://www.techrepublic.com/article/enable-podman-sudoless-container-management/
_c
sudo bash -c "cat >/etc/sysctl.d/userns.conf" <<!
user.max_user_namespaces=65536
!

sudo sysctl -p /etc/sysctl.d/userns.conf

true
}


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
function _podman_registry_client() {  # <- $DOCKER_REGISTRY_ENDPOINT
echo TODO
:<<\_j

_url_parse $DOCKER_REGISTRY_ENDPOINT DOCKER_REGISTRY  # -> ${DOCKER_REGISTRY[scheme]}, ${DOCKER_REGISTRY[hostname]}, ${DOCKER_REGISTRY[port]}

DOCKER_REGISTRY=${DOCKER_REGISTRY[host]:-80}

_debug "{DOCKER_REGISTRY[host]}: ${DOCKER_REGISTRY[host]}"

case "${DOCKER_REGISTRY[scheme]}" in

https*)
#_secure_private_registry_client
sudo bash -c "cat > $SYSTEM_CONFIG_FILE" <<!
[registries.secure]
registries = [ $DOCKER_REGISTRY ]
!
;;

http*)
#_insecure_private_registry_client
sudo bash -c "cat > $SYSTEM_CONFIG_FILE" <<!
[registries.insecure]
registries = [ $DOCKER_REGISTRY ]
!
;;

esac
_j
false
}


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
:<<\_c
Pushing to a registry configured as a pull-through cache is unsupported.
_c

function _podman_mirror_client() {

_debug "_podman_mirror_client"

[[ -z "$DOCKERHUB_ENDPOINT" ]] && _return '[[ -z "$DOCKERHUB_ENDPOINT" ]]'

_url_parse $DOCKERHUB_ENDPOINT DOCKERHUB  # -> DOCKERHUB[*]

:<<\_c
"${DOCKERHUB_ENDPOINT#*://}"  # strip preceding 'http://' or 'https://'
_c

case ${DOCKERHUB[scheme]} in
http) insecure=true;;
https) insecure=false;;
*) _error "${DOCKERHUB[scheme]} scheme unknown"; exit 1;;
esac

:<<\_s
# system-wide

sudo mkdir -p $SYSTEM_CONFIG_FILE.d

sudo bash -c "cat > $SYSTEM_CONFIG_FILE.d/dockerhub.conf" <<!
unqualified-search-registries = ['${DOCKERHUB_ENDPOINT#*://}']

[[registry.mirror]]
location="${DOCKERHUB_ENDPOINT#*://}"
insecure=$insecure
!
_s

:<<\_j
cat > $USER_CONFIG_FILE <<!
unqualified-search-registries = ['${DOCKERHUB_ENDPOINT#*://}']

[[registry]]
location="docker.io"
insecure=false

[[registry.mirror]]
location="${DOCKERHUB_ENDPOINT#*://}"
insecure=$insecure
!
_j

:<<\_j
cat > $USER_CONFIG_FILE <<!
unqualified-search-registries = ["${DOCKERHUB[host]}"]

[[registry]]
prefix="docker.io"
location="${DOCKERHUB[host]}"
insecure=$insecure

[[registry.mirror]]
location="${DOCKERHUB[host]}"
insecure=$insecure
_j

:<<\_c
the system config suffices for the user config too
system config supports both system and user sessions/services
_c

TYPE=system  # system|user

case $TYPE in

system)

sudo mkdir -p /etc/containers

sudo bash -c "cat > /etc/containers/registries.conf" <<!
unqualified-search-registries = ["docker.io"]

[[registry]]
prefix = "docker.io"
insecure = false
location = "${DOCKERHUB_ENDPOINT#*://}"

[[registry.mirror]]
location = "${DOCKERHUB_ENDPOINT#*://}"
insecure = $insecure
!
;;

user)

mkdir -p ~/.config/containers

cat > ~/.config/containers/registries.conf <<!
unqualified-search-registries = ["docker.io"]

[[registry]]
prefix = "docker.io"
insecure = false
location = "${DOCKERHUB_ENDPOINT#*://}"

[[registry.mirror]]
location = "${DOCKERHUB_ENDPOINT#*://}"
insecure = $insecure
!
;;

esac

#unqualified-search-registries = ['${DOCKERHUB_ENDPOINT#*://}']

true
}

:<<\_c
https://stackoverflow.com/questions/68468107/kubernetes-pull-from-local-registry-not-working-how-to-fix-that

[registries.search]
registries = ['registry1.com', 'registry2.com']

[registries.insecure]
registries = ['registry3.com']
_c