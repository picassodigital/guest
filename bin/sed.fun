
:<<\_c
. sed.fun

devstack's iniset would insert content at the very beginning of sections
devstack's local.conf/localrc execute content sequentially
this posed a problem for adding new lines to localrc, because new commands that should be appended were always prepended
the following hack relies on a comment added to the section that indicates its end
new content is prepended just before this terminator, which is to say, it is appended to the bottom of the existing section's content just like we want
_c
:<<\_c
address space is '/$lead/,/$tail/'
braces denote compound sed commands to apply to address space
append(a) and insert(i) must be followed by a newline, then the content, then another newline
_c


# ----------
function _section_insert_before_tail() {
# $1: lead
# $2: tail
# $3: destination file

insert=$(while read i; do [[ -n "$i" ]] && printf "%s\\\n" "$i"; done)

grep -q "$1" $3

if [[ $? -eq 0 ]]; then

:<<\_s
# this would fail with some file permission problem
sed -i "/$1/,/$2/{ /$1/n; /$2/i$insert
}" $3
_s
sed "/$1/,/$2/{ /$1/n; /$2/i$insert
}" $3 1<> $3

else

cat << EOF >> $3
$1
$insert
EOF

fi
}


# ----------
:<<\_c
replace a string with a match in a file - does nothing if no match was found 
_replace <old string> <new string> <file>
_replace <old string> <new string> <section> <file>

_replace "^auth_host.*" "#&" /etc/glance/glance-api.conf
_c

function _replace() {
if [[ "$#" -eq 3 ]]; then
sed -i "s~$1~$2~g" $3
elif [[ "$#" -eq 4 ]]; then
sed -i "/$3/,/^\[/{ s~$1~$2~g }" $4
fi
}


# ----------
function _replace_insert() {
if grep -q "^$1" "$3"; then

if [[ "$#" -eq 3 ]]; then
sed -i "s~$1~$2~g" $3
elif [[ "$#" -eq 4 ]]; then
sed -i "/$3/,/^\[/{ s~$1~$2~g }" $4
fi

else
echo "$2" >> "$3"
fi
}


# ----------
:<<\_c
append a string after the match in a file - does nothing if no match was found 
_append <string> <match> <file>

_append "admin_token=$ADMIN_TOKEN" "^\[DEFAULT\]" /etc/keystone/keystone.conf
_c

function _append() {
sed -i "/$2/ a$1" $3
}


# ----------
function _get_value() {

v=$(grep $1 $2) || return 1
echo -e $v | awk -F "=" '{print $2}' | sed -e 's/^[[:space:]]*//'
}


# ----------
true
