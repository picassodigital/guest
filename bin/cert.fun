:<<\_c
. $PR/$PV/guest/bin/cert.fun
_c
:<<\_x
. cert.fun
_x

CERTBOT_HOME=${CERTBOT_HOME:-/etc/letsencrypt}

sudo mkdir -p $CERTBOT_HOME

:<<\_s
FTP_USER=${FTP_USER:-uftp}
FTP_PASS=${FTP_PASS:-uftp}
_s


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
function _cert_valid() {
local pathfile=$1

_debug "_cert_valid - pathfile: $pathfile"

local file_date_in_seconds=$(date --utc --reference=$pathfile +%s)

local delta=$(($(date --utc +%s) - $file_date_in_seconds))
local ninety_days=$((90*24*60*60))
local fourtyfive_days=$((90*24*60*60/2))
local expired=$(($delta > $fourtyfive_days))

_debug "delta: $delta, ninety_days: $ninety_days, expired: $expired"

(( $expired == 0 ))  # return $?
}


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
:<<\_c
$1 - DOMAIN
_c

:<<\_s
function _cert_fetch() {  # <- $FTP_USER, $FTP_PASS, $CERT_SERVER -> stdout

curl -s -o /tmp/letsencrypt.tar.gz ftp://${FTP_USER}:${FTP_PASS}@$CERT_SERVER/$(_reverse_domain_path $1)/letsencrypt.tar.gz && {

echo "/tmp/letsencrypt.tar.gz"  # return pathfile
}

# return stdout
}
_s


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
:<<\_c
$1 - DOMAIN
$2 - pathfile

--directory=$dest_folder - without this then tar unarchiving is relatve to '/'
restoring to original condition requires writing to $CERTBOT_HOME/{archive,live)/$DOMAIN
_c

function _cert_restore() {
local pathfile=$2

_debug "sudo tar --directory=$CERTBOT_HOME -xvpzf $pathfile"

sudo tar --directory=$CERTBOT_HOME -xvpzf "$pathfile"

# return $?
}
