:<<\_c
[usage]
. $PR/$PV/guest/bin/dns.fun

_dns_set test.picasso.digital. 1.2.3.4

VDOM=bit.cafe; _dns_set test 1.2.3.4
VDOM=bit.cafe; _dns_lookup test.$VDOM
nslookup test.$VDOM

PNAME=kv
_dns_set ${PNAME}.bit.cafe. 10.0.0.12

VDOM=bit.cafe _dns_set $PNAME 10.0.0.12
VDOM=picasso.digital _dns_lookup identity

VDOM=bit.cafe {VDOM}_DNS_KEY_FILE=$NAMESP_CTX/${VDOM}.key _dns_set $PNAME 10.0.0.12

_dns_set $PNAME $ip
VDOM=bit.cafe {VDOM}_DNS_KEY_FILE=$NAMESP_CTX/${VDOM}.key _dns_set $PNAME $ip
VDOM=bit.cafe {VDOM}_DNS_KEY_FILE=$NAMESP_CTX/${VDOM}.key _dns_set test 192.168.1.254

_dns_lookup ${PNAME}${VDOM:+.$VDOM}
_c

_debug "DESKSP/$PV/guest/bin/dns.fun DNS_ENDPOINT: $DNS_ENDPOINT"


# ---------- ----------
function _dns_lookup() {

[[ -z "$@" ]] && { echo "<FQDN>"; return 1; }

:<<\_c
unset or empty DNS_ENDPOINT implies that the dns should be handled dynamically by fetching its value from KV each time it is needed
_c
[[ -z "$DNS_ENDPOINT" ]] && {

_debug_dns "DNS_ENDPOINT=\$(_kv_get DNS_ENDPOINT)"

local DNS_ENDPOINT=$(_kv_get DNS_ENDPOINT) || _return "_kv_get DNS_ENDPOINT"  # <- $VDOM
}

_debug_dns "dns.fun> DNS_ENDPOINT: $DNS_ENDPOINT"

_url_parse $DNS_ENDPOINT DNS || _return "_url_parse $DNS_ENDPOINT DNS"  # -> DNS[*]

local fqdn_dot=$1

[[ "$fqdn_dot" == *.* ]] || {  # does it include a period (test.picasso.digital.)?

# it does not include a period

[[ -n "$VDOM" ]] && fqdn_dot+="${VDOM:+.$VDOM}."  # append custom search
}

local r="$(nslookup $fqdn_dot ${DNS[hostname]})"

[[ $? -eq 0 ]] || _return "DNS entry not found for $1"

r=$(echo "$r" | awk -F':' '/^Address: / {matched = 1} matched {print $2}')
[[ -n "$r" ]] || return 1

echo $r  # return $r
}
export -f _dns_lookup


# ---------- ----------
# copied from host/bin/dns.fun...

function _dns_set() {  # <- $VDOM
local VDOM=$VDOM
local fqdn_dot=$1

_debug_dns "_dns_set @: $@, VDOM: $VDOM"

[[ "${DNS_DISABLE^^}" == 'TRUE' ]] && {

_warn "_dns_set - DNS_DISABLE: $DNS_DISABLE"
return 0
}

[[ -z "$1" || -z "$2" ]] && {
echo "_dns_set $@"
1>&2 eecho "[usage}: _dns_set <FQDN> <IP>"
return 1
}

if [[ "$fqdn_dot" == *.* ]]; then  # does it include a period?

# yes, it does include a period

# fqdn_dot=test.picasso.digital.

if [[ "$fqdn_dot" == *. ]]; then  # does it end with a period?

# yes, it ends with a period

VDOM=${fqdn_dot::-1}  # strip trailing '.' - VDOM=test.picasso.digital
VDOM=${VDOM#*.}  # strip hostname - VDOM=picasso.digital

else

VDOM=${fqdn_dot#*.}  # strip hostname - VDOM=picasso.digital

fqdn_dot+="."  # fqdn_dot=test.picasso.digital.

fi

else

# no, it does not include a period - 

[[ -n "$VDOM" ]] && fqdn_dot+="${VDOM:+.$VDOM}."  # append custom search

fi

:<<\_c
unset or empty DNS_ENDPOINT implies that the dns should be handled dynamically by fetching its value from KV each time it is needed
_c
[[ -z "$DNS_ENDPOINT" ]] && {

_debug_dns "DNS_ENDPOINT=\$(_kv_get DNS_ENDPOINT)"

local DNS_ENDPOINT=$(_kv_get DNS_ENDPOINT) || _return "_kv_get DNS_ENDPOINT"  # <- $VDOM
}

_debug_dns "dns.fun> DNS_ENDPOINT: $DNS_ENDPOINT"

_url_parse $DNS_ENDPOINT DNS || _return "_url_parse $DNS_ENDPOINT DNS"  # -> DNS[*]

:<<\_x
fqdn=www.picasso.digital
ip=1.2.3.4
_x

_debug_dns "s=\$(VDOM=$VDOM _kv_get DNS_KEY)"

local s=$(VDOM=$VDOM _kv_get DNS_KEY); [[ -z "$s" ]] && _return "KV not found 'VDOM=$VDOM _kv_get DNS_KEY'"  # <- $VDOM
:<<\_x
vdom=runnable.org
VDOM=$vdom _kv_get DNS_KEY
VDOM= _kv_get ${vdom//./_}DNS_KEY
_x


(( DEBUG_DNS > 0 )) && {
echo "cat <<! | nsupdate -k <(echo $s)
server ${DNS[hostname]}
update delete $fqdn_dot A
update add $fqdn_dot 3600 A $2
send
!
"
}

cat <<! | nsupdate -k <(echo $s)
server ${DNS[hostname]}
update delete $fqdn_dot A
update add $fqdn_dot 3600 A $2
send
!

}


# ---------- ----------
function _dns_delete() {

[[ -z "$1" ]] && {
echo "_dns_delete $@"
1>&2 printf "\e[1;41m%s\e[0m\n" "[usage}: _dns_delete <FQDN>"
return 1
}

[[ -z "$DNS_HOST" ]] && _warn "DNS_HOST is not set"

local key_name

local fqdn_dot=$1
local key s

if [[ "$fqdn_dot" == *.* ]]; then  # does it include a period?

# fqdn_dot=test.picasso.digital

if [[ "$fqdn_dot" == *. ]]; then  # does it end with a period?

# fqdn_dot=test.picasso.digital.

key=${fqdn_dot::-1}  # strip trailing '.'

# key=test.picasso.digital

key=${key#*.}DNS_KEY

else

key=${fqdn_dot#*.}DNS_KEY

fqdn_dot+="."  # fqdn_dot=test.picasso.digital.

fi

else

# it does not include a period

[[ -n "$VDOM" ]] && fqdn_dot+="${VDOM:+.$VDOM}."  # append custom search

key=${VDOM:+${VDOM}}DNS_KEY

fi

s=$(_kv_get $key); [[ -z "$s" ]] && _return "$key not found"

cat <<! | nsupdate -k <(echo $s)
server $DNS_HOST
update delete $fqdn_dot A
send
!

}
