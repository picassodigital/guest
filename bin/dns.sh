[[ -v DEBUG && $DEBUG -ge 0 ]] && printf "\e[1;38;5;228m>>> %s @: %s\e[0m\n" "${BASH_SOURCE[0]}" "${*@Q}"

:<<\_c
$PR/$PV/guest/bin/dns.sh [dest] [KV_ENDPOINT]

configure the dns runtime

see: $PROOF/dns/
_c

:<<\_x
DNS_ENDPOINT=bind://$(_ip_lookup MNIC_NAMESERVER1):53
. dns.sh  # <- $DNS_ENDPOINT -> $PR/bin/{100-dns.env,101-dns.fun}
_x

#echo sdsfdslfsf
#DEBUG=1

_debug_dns "dns.sh @: $@"
_debug_dns "$(declare -p DNS)"

PR_BIN=${1:-$PR/bin}

DNS_ENDPOINT=${2:-$DNS_ENDPOINT}  # argument takes precidence

_debug_dns "DNS_ENDPOINT=$DNS_ENDPOINT"
[[ -n "$DNS_ENDPOINT" ]] || _return '[[ -n "$DNS_ENDPOINT" ]]'

_url_parse $DNS_ENDPOINT DNS || _return "_url_parse $DNS_ENDPOINT DNS"  # -> DNS[]


#if [[ "${DNS[hostname]}" =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then  # explicit ip?

#DNS[hostname]=${DNS[hostname]}
#fi

# DNS[hostname] must be an ip so that no dns server is required to resovle it
[[ -n "${DNS[hostname]}" ]] || _exit '[[ -n "${DNS[hostname]}" ]]'

_debug_dns "$(declare -p DNS)"


# ----------
case "${DNS[scheme]}" in

bind)

cat > $PR_BIN/100-dns.env <<!
DNS_ENDPOINT=$DNS_ENDPOINT
_url_parse \$DNS_ENDPOINT DNS
!

ln -sf $(which dns.fun) $PR_BIN/101-dns.fun  # autoload

. $PR_BIN/100-dns.env
. $PR_BIN/101-dns.fun

true
;;

*) _error "Unknown DNS[scheme]: ${DNS[scheme]}"; false;;
esac


# ----------
#return $?
