:<<\_c
. longopts.sh  # reads arguments preceded with '--' until the first argument not preceded with '--'
argument not preceded with '--' -> longargs[]
arguments preceded with '--' -> longopts[], longvals[]
remaining argument -> @

see: $PR/notes/longopts.md
_c


#echo "longopts.sh *: $*"

# marshal commandline arguments/values into the bash array: longopts
declare -a longopts=()
declare -a longvals=()
declare -a longargs=()

optspec=":v-:"  # ':' - leading colon turns on silent error reporting, 'v' first so DEBUG is set early, trailing ':' means to expect a parameter

# options are contained in $@

trigger=false

:<<\_c
quotes are necessary to preserve inner quotes
take the following cli command
ubuntu 22.04.4 virtualbox amd64 ubuntu-22.04.4-live-server-amd64.iso ""

for OPTARG in "$@"  # this will preserve the double-quote argument and simply consider it's value as blank

for OPTARG in $@  # this would consume the double-quote argument as though it never existed
_c

for OPTARG in "$@"; do  # quotes are necessary

#echo "longopts OPTARG: $OPTARG"

  case "${OPTARG}" in
    --)
      shift
      trigger=true  # flag that options have been encountered which stops the processing of further arguments

#echo "--) $OPTARG, @: $@"

      break;;

    --*)
      shift
      trigger=true  # flag

#echo "--*) $OPTARG, @: $@"

#echo "OPTARG:  ${OPTARG[*]}"
#echo "OPTIND:  ${OPTIND[*]}"

      OPTARG="${OPTARG:2}"  # remove '--'

# is there a value on the right side of the '='
if [[ "$OPTARG" =~ '=' ]]; then

  # yes, there is an '='

      val=${OPTARG#*=}  # split on '='
      opt=${OPTARG%=$val}

      longopts+=("$opt")
      longvals+=("$val")
else
      longopts+=($OPTARG)
      longvals+=("")
fi
      ;;

    *)

#echo "*) $OPTARG, @: $@"

if $trigger; then

#echo "break"

      break

else
      shift
      longargs+=($OPTARG)
fi
      ;;

    esac

done

#echo "@: $@"

#echo "longopts.sh - set -- \"${other[@]}\""

#set -- "${other[@]}"  # restore other parameters
#other+=" $@"

#echo "fini @: $@"
#echo "fini $(declare -p longopts)"
#echo "fini $(declare -p longvals)"
#echo "fini $(declare -p longargs)"

:<<\_c
only if there are options should 'help' be in $@
if there are no options 'help' is in longargs[]
_c

#if [[ ${#longopts[@]} -gt 0 ]]; then

#echo "longopts set -- $@"

set -- $@

#echo "longopts @: $@"

#else

#set -- ${longargs[@]} $@
#longargs=()

#fi

true
