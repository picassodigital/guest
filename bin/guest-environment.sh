[[ -v DEBUG && $DEBUG -ge 0 ]] && printf "\e[1;38;5;228m>>> %s @: %s\e[0m\n" "${BASH_SOURCE[0]}" "${*@Q}"

:<<\_c
. guest-environment.sh

see: $PROOF/picasso-init/picasso-init.md
see: $PROOF/picasso/$PV/guest/bootstrap.md

guests load their environment from two sources...
1) _source_env $PR/$PV/bin/ - core environment (basebox.tar/instance.tar)
2) _source_env $PR/bin/ - system provisioning environment
#3) _source_env ~/.picasso/bin/ - user provisioning environment
_c

:<<\_x
. guest-environment.sh
_x

#echo "guest-environment> @: $@"


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
# source guest environment

#echo "guest-environment> @: $@"


# ----------
function _source_env() {

##echo "$(ls -l $1)"

for script in $(/usr/bin/find $1 -maxdepth 1 -name '[0-9]*.*' \( -type l -o -type f \) | /usr/bin/sort); do

#echo "guest-environment/_source_env> script: $script"

. $script || _return ". $script"

#echo "guest-environment/_source_env> 2 script: $script"

done

true

}


# ----------
#echo "guest-environment> _source_env $PR/$PV/bin/"  # no _debug() yet

_source_env $PR/$PV/bin/ || return 1  # ro - first, load disto functionality -> _debug()

#echo "guest-environment> _source_env $PR/bin/"

_source_env $PR/bin/ || return 1  # rw - second, load system configuration

[[ -d ~/.picasso/bin ]] && {

#echo "guest-environment> _source_env ~/.picasso/bin/"

_source_env ~/.picasso/bin/ || return 1  # third, load user configuration
}

#echo "guest-environment> done"


# ----------
true
