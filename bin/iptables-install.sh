#!/usr/bin/env bash
[[ -v DEBUG && $DEBUG -ge 0 ]] && printf "\e[1;38;5;228m>>> %s @: %s\e[0m\n" "${BASH_SOURCE[0]}" "${*@Q}"

:<<\_c
. iptables-install.sh [mode]

https://gist.github.com/thomasfr/9712418
http://ubuntuforums.org/archive/index.php/t-1276011.html
https://en.wikipedia.org/wiki/List_of_TCP_and_UDP_port_numbers
_c

:<<\_x
FIREWALL=iptables://input-drop-forward-drop-output-accept
. iptables-install.sh $FIREWALL
_x

TEST=${TEST:-false}
LOG=true

echo "iptables-install.sh @: $@"
DEBUG=2
DEBUG_FIREWALL=1
_debug_firewall "iptables-install.sh @: $@"

:<<\_c
iptables-install.sh declares _tcp_input_accept, etc that configures the INPUT chain
iptables.fun declares _tcp_in_accept, etc that configures the PICASSO_INPUT chain
_c

:<<\_c
# see port changes
service smb stop
netstat -ln > netstat-ln-smb.before
service smb start
netstat -ln > netstat-ln-smb.after
diff netstat-ln-smb.*
_c

:<<\_c
when this script first executes we have network access properly functioning from vagrant
this script then purges vagrant's configuration (ssh and samba) and immediately replaces it

iptable chains are ORDERED lists where the first matching rule is run
rules may be inserted (with offset) or appended
inserting rules brings with it the issue of messing up the order - ie: you always want the most frequently utilized rule first in the list
appending rules brings with it the issue of appending a rule after another rule that terminates the chain - ie: after 'iptables -A INPUT -j DROP'
we can get around these issues by creating user-defined chains that are embedded within the default chain
rules are then appended to these user-defined chains
user-defined chains maintain the order of the default chain
this script creates a default chain that does the following...
1) primary rules
2) calls user-defined chains
3) logging rules
4) accepts or rejects all traffic depending on policy

user-defined chains:
PICASSO_INPUT
PICASSO_OUTPUT

the user-defined chains may then be flushed and appended to for testing

iptables://input-accept-forward-accept-output-accept...
sudo bash -c "iptables -F; iptables -X; iptables -t nat -F; iptables -t nat -X; iptables -t mangle -F; iptables -t mangle -X; iptables -P INPUT ACCEPT; iptables -P FORWARD ACCEPT; iptables -P OUTPUT ACCEPT"

iptables://input-drop-forward-drop-output-accept...
sudo bash -c "iptables -F; iptables -X; iptables -t nat -F; iptables -t nat -X; iptables -t mangle -F; iptables -t mangle -X; iptables -P INPUT DROP; iptables -P FORWARD DROP; iptables -P OUTPUT ACCEPT"

iptables://input-drop-forward-drop-output-accept +ssh...
sudo bash -c "iptables -F; iptables -X; iptables -t nat -F; iptables -t nat -X; iptables -t mangle -F; iptables -t mangle -X; iptables -P INPUT DROP; iptables -P FORWARD DROP; iptables -P OUTPUT ACCEPT; \
 iptables -A INPUT -p tcp --dport 22 -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "ssh-server" -j ACCEPT; \
 iptables -A OUTPUT -p tcp --sport 22 -m conntrack --ctstate ESTABLISHED -m comment --comment "ssh-server" -j ACCEPT; \
"


sudo iptables -L

sudo iptables -F
sudo iptables -X
sudo iptables -t nat -F
sudo iptables -t nat -X
sudo iptables -t mangle -F
sudo iptables -t mangle -X
sudo iptables -P INPUT ACCEPT
sudo iptables -P FORWARD ACCEPT
sudo iptables -P OUTPUT ACCEPT

iptables -N PICASSO_INPUT
iptables -N PICASSO_OUTPUT
iptables -A PICASSO_INPUT -j ACCEPT
iptables -A PICASSO_OUTPUT -j ACCEPT

sudo iptables -I PICASSO_INPUT -j ACCEPT
sudo iptables -I PICASSO_OUTPUT -j ACCEPT

sudo journalctl -f | grep 'PICASSO_INPUT\|PICASSO_OUTPUT'

sudo iptables -D PICASSO_INPUT -j ACCEPT
sudo iptables -D PICASSO_OUTPUT -j ACCEPT

drop policy...
iptables -F PICASSO_INPUT
iptables -F PICASSO_OUTPUT
iptables -A PICASSO_INPUT -j DROP
iptables -A PICASSO_OUTPUT -j DROP

ACCEPT(ing) or DROP(ing) a packet terminates its rule matching
packets traverse the chains until they are either ACCEPT(ed) or DROP(ed) or the end of the chains is reached

1) to find PC's running windows 8 and windows 7: UDP 3702, UDP 5355, TCP 5357, TCP 5358.
2) to find PC's running earlier version of windows: UDP 137, UDP 138:, TCP 139 - NetBIOS over TCP/IP aka NetBT aka netbios-ssn - (Windows NT)
, TCP 445 - SMB/CIFS over IP (Windows 2000+)
, UDP 5355.
3) to find other network devices I enabled: UDP 1900, TCP 2869, UDP 3702, UDP 5355, TCP 5357, TCP 5358.

usage:

. iptables-install.sh
_iptables://input-drop-forward-drop-output-drop
iptables -F PICASSO_INPUT
iptables -F PICASSO_OUTPUT
#_iptables "-A PICASSO_INPUT -p tcp -i $MNIC_IFACE -s 192.168.1.0/24 --dport 445 -m conntrack --ctstate NEW -j ACCEPT"
_iptables "-A PICASSO_INPUT -p tcp -s 192.168.1.0/24 --dport 445 -m conntrack --ctstate NEW -j ACCEPT"
ls $PR/$PV/install
iptables-save > /etc/sysconfig/iptables  # iptables will be restored at restart

NEW - The connection has not yet been seen.
RELATED - The connection is new, but is related to another connection already permitted.
ESTABLISHED - The connection is already established.
INVALID - The traffic couldn't be identified for some reason.

REJECT target will send a reply icmp packet to the source system telling that system that the packet has been rejected. By default the message will be "port is unreachable".
REJECT target is vulnerable to DoS style attacks
DROP target simply drops the packet without sending any reply packets back.

http://crm.vpscheap.net/knowledgebase.php?action=displayarticle&id=29
_c

:<<\_c
find /var/log -mmin 1  # find any file modified in the last 1 min inside the /var/log and below

grep 'IPT ' /var/log/kern.log
grep 'IPT ' /var/log/syslog
_c

:<<\_c
sudo bash -c "cat > /etc/rsyslog.d/00-my_iptables.conf" <<!
:msg,contains,"IPT " -/var/log/iptables.log
& stop
!

sudo systemctl restart rsyslog.service

telnet 192.168.1.32 1234  # access a blocked port to generate a log entry

cat /var/log/iptables.log  # view the logs that result from the above access
_c


#[[ -n "$LAN" ]] && nic=nic$LAN && NIC=${!nic}

IPT="sudo iptables"

alias _tcp_input_accept="$IPT -A INPUT -p tcp -j ACCEPT "
alias _tcp_output_accept="$IPT -A OUTPUT -p tcp -j ACCEPT "
alias _udp_input_accept="$IPT -A INPUT -p udp -j ACCEPT "
alias _udp_output_accept="$IPT -A OUTPUT -p udp -j ACCEPT "
alias _tcp_input_drop="$IPT -A INPUT -p tcp -j DROP "
alias _tcp_output_drop="$IPT -A OUTPUT -p tcp -j DROP "
alias _udp_input_drop="$IPT -A INPUT -p udp -j DROP "
alias _udp_output_drop="$IPT -A OUTPUT -p udp -j DROP "



# Your DNS servers you use: cat /etc/resolv.conf
#DNS_SERVER="192.168.1.10 8.8.8.8"
PACKAGE_SERVER=${PACKAGE_SERVER:-0.0.0.0/0}  # "ftp.us.debian.org security.debian.org"

_warn "PACKAGE_SERVER: $PACKAGE_SERVER"

:<<\_j
if [[ -v DEBUG && $DEBUG -ge 1 ]]; then
alias _tcp_input_accept='iptables -A INPUT -p tcp -j ACCEPT -m comment --comment "$_TOP_:$LINENO" '
alias _tcp_output_accept='iptables -A OUTPUT -p tcp -j ACCEPT -m comment --comment "$_TOP_:$LINENO" '
alias _udp_input_accept='iptables -A INPUT -p udp -j ACCEPT -m comment --comment "$_TOP_:$LINENO" '
alias _udp_output_accept='iptables -A OUTPUT -p udp -j ACCEPT -m comment --comment "$_TOP_:$LINENO" '
else
_j
#alias _tcp_input_accept='iptables -A INPUT -p tcp -j ACCEPT '
#alias _tcp_output_accept='iptables -A OUTPUT -p tcp -j ACCEPT '
#alias _udp_input_accept='iptables -A INPUT -p udp -j ACCEPT '
#alias _udp_output_accept='iptables -A OUTPUT -p udp -j ACCEPT '

. iptables.fun
:<<\_j
fi
_j

_debug_firewall lsdflsdlf

# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
function _iptables_provision() {

_debug_firewall "OS: $OS"

case $OS in

redhat|fedora)

_install iptables-services

# disable firewalld
#systemctl -q is-active firewalld.service && systemctl stop firewalld.service
systemctl disable firewalld.service --now &>/dev/null
systemctl mask firewalld.service &>/dev/null

# Create this or starting iptables will fail
touch /etc/sysconfig/iptables

# enable iptables
systemctl unmask iptables.service
systemctl unmask ip6tables.service
systemctl start iptables.service
systemctl start ip6tables.service
systemctl enable iptables.service
systemctl enable ip6tables.service

:<<\_x
systemctl is-active iptables.service
systemctl status iptables.service
systemctl stop iptables.service

systemctl status firewalld.service
_x
;;

debian|ubuntu)

# iptables is a kernel module and not a service in ubuntu!
# to create a service for it see: http://serverfault.com/questions/129086/how-to-start-stop-iptables-on-ubuntu

sudo debconf-set-selections <<!
iptables-persistent iptables-persistent/autosave_v4 boolean true
iptables-persistent iptables-persistent/autosave_v6 boolean true
!

_debug_firewall "$(type _install)"

if &>/dev/null _install iptables-persistent; then  # this removes ufw
#if _install iptables-persistent; then  # this removes ufw

mkdir -p /etc/iptables
#chmod go-rwx /etc/iptables

:<<\_x
systemctl is-active ufw.service
systemctl status ufw.service
systemctl stop ufw.service
_x

else

_exit "_install iptables-persistent"
fi
;;

*)
_exit "TODO: $OS"
;;

esac

}


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
function _build_default_chain() {  # <- [$IFACE $CIDR]

_debug_firewall "_build_default_chain> @: $@"

:<<\_j
[[ -n "$MNIC_IFACE" ]] && {
trusted_iface=$MNIC_IFACE
trusted_cidr=$MNIC_CIDR
}
_j

:<<\_j
[[ -v IFACES[MNIC] ]] && {

declare -n spec=${IFACES[MNIC]}
trusted_iface=${spec[iface]}
trusted_cidr=${spec[cidr]}
}
_j
trusted_iface=$1
trusted_cidr=$2

_debug_firewall "_build_default_chain> trusted_iface: $trusted_iface, trusted_cidr: $trusted_cidr"

# Allow Established Sessions
_tcp_input_accept -m conntrack --ctstate ESTABLISHED,RELATED
_tcp_output_accept -m conntrack --ctstate ESTABLISHED

_udp_input_accept -m conntrack --ctstate ESTABLISHED,RELATED
#_udp_output_accept -m conntrack --ctstate ESTABLISHED,RELATED
_udp_output_accept -m conntrack --ctstate ESTABLISHED

# echo "allow all and everything on localhost"
#sudo iptables -A INPUT -i lo -s 127.0.0.1 -d 127.0.0.1 -j ACCEPT
#sudo iptables -A OUTPUT -o lo -s 127.0.0.1 -d 127.0.0.1 -j ACCEPT
sudo iptables -A INPUT -i lo -j ACCEPT
sudo iptables -A INPUT ! -i lo -s 127.0.0.0/8 -j REJECT  # https://unix.stackexchange.com/questions/395328/iptables-rule-for-loopback
sudo iptables -A OUTPUT -o lo -j ACCEPT

sudo iptables -A INPUT -m conntrack --ctstate INVALID -j DROP

# ----------
## This should be one of the first rules so dns lookups are already allowed for your other rules
# http://www.cyberciti.biz/tips/linux-iptables-12-how-to-block-or-open-dnsbind-service-port-53.html
:<<\_x
cat <<EOF >> /etc/resolv.conf
nameserver 8.8.8.8
EOF

for ip in $(grep nameserver /etc/resolv.conf | awk '{print $2}')
do
echo "nameserver: $ip"
done
_x
:<<\_x
cat /etc/resolv.conf
_x

#for ip in $DNS_SERVER

for ip in $(grep "^nameserver" /etc/resolv.conf | awk '{print $2}'); do

#iptables -A OUTPUT -p tcp --dport 53 -m conntrack --ctstate NEW -m comment --comment "$_TOP_:$LINENO" -j ACCEPT
#iptables -A OUTPUT -p udp --sport 53 -m conntrack --ctstate NEW -m comment --comment "$_TOP_:$LINENO" -j ACCEPT

#iptables -A OUTPUT -p udp --sport 1024:65535 -d $ip --dport 53 -m state --state NEW,ESTABLISHED -j ACCEPT
#iptables -A INPUT -p udp -s $ip --sport 53 --dport 1024:65535 -m state --state ESTABLISHED -j ACCEPT
#iptables -A OUTPUT -p tcp --sport 1024:65535 -d $ip --dport 53 -m state --state NEW,ESTABLISHED -j ACCEPT
#iptables -A INPUT -p tcp -s $ip --sport 53 --dport 1024:65535 -m state --state ESTABLISHED -j ACCEPT

_debug_firewall "Allowing outgoing (client) DNS lookups (tcp, udp port 53) to server '$ip'"
#	sudo iptables -A OUTPUT -p udp -d $ip --dport 53 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT -m comment --comment "$_TOP_:$LINENO"
#	sudo iptables -A INPUT  -p udp -s $ip --sport 53 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT -m comment --comment "$_TOP_:$LINENO"
#sudo iptables -A OUTPUT -p udp --dport 53 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT -m comment --comment "$_TOP_:$LINENO"
#sudo iptables -A INPUT  -p udp --sport 53 -m conntrack --ctstate ESTABLISHED -j ACCEPT -m comment --comment "$_TOP_:$LINENO"

:<<\_c
http://www.cyberciti.biz/tips/linux-iptables-12-how-to-block-or-open-dnsbind-service-port-53.html
SERVER_IP="202.54.10.20"
DNS_SERVER="202.54.1.5 202.54.1.6"
for ip in $DNS_SERVER
do
iptables -A OUTPUT -p udp -s $SERVER_IP --sport 1024:65535 -d $ip --dport 53 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A INPUT -p udp -s $ip --sport 53 -d $SERVER_IP --dport 1024:65535 -m state --state ESTABLISHED -j ACCEPT
iptables -A OUTPUT-p tcp -s $SERVER_IP --sport 1024:65535 -d $ip --dport 53 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A INPUT -p tcp -s $ip --sport 53 -d $SERVER_IP --dport 1024:65535 -m state --state ESTABLISHED -j ACCEPT
done
_c
SERVER_IP=$ip
#SERVER_IP=0/0
_udp_output_accept --sport 1024:65535 -d $ip --dport 53 -m state --state NEW,ESTABLISHED
_udp_input_accept -s $ip --sport 53 --dport 1024:65535 -m state --state ESTABLISHED
_tcp_output_accept --sport 1024:65535 -d $ip --dport 53 -m state --state NEW,ESTABLISHED  # apparently Google requires this?
_tcp_input_accept -s $ip --sport 53 --dport 1024:65535 -m state --state ESTABLISHED  # apparently Google requires this?


# not sure if tcp is necessary
#	sudo iptables -A OUTPUT -p tcp --sport 1024:65535 -d $ip --dport 53 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT -m comment --comment "$_TOP_:$LINENO"
#	sudo iptables -A INPUT  -p tcp -s $ip --sport 53 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT -m comment --comment "$_TOP_:$LINENO"
#_tcp_out_accept --dport 53 -m conntrack --ctstate NEW
#_udp_out_accept --dport 53 -m conntrack --ctstate NEW

#sudo iptables -A OUTPUT -p udp --dport 53 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT
#sudo iptables -A OUTPUT -p tcp --dport 53 -m state --state NEW,ESTABLISHED -j ACCEPT

#	sudo iptables -A OUTPUT -p udp -d $ip --dport 53 -m comment --comment "$_TOP_:$LINENO" -j ACCEPT
#	sudo iptables -A INPUT  -p udp -s $ip --sport 53 -m comment --comment "$_TOP_:$LINENO" -j ACCEPT
#	sudo iptables -A OUTPUT -p tcp -d $ip --dport 53 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT
#	sudo iptables -A INPUT  -p tcp -s $ip --sport 53 -m conntrack --ctstate ESTABLISHED     -j ACCEPT
done

:<<\_s
SERVER_IP=0/0
_udp_output_accept -s $SERVER_IP --sport 1024:65535 --dport 53 -m state --state NEW,ESTABLISHED
_udp_input_accept --sport 53 -d $SERVER_IP --dport 1024:65535 -m state --state ESTABLISHED
_tcp_output_accept -s $SERVER_IP --sport 1024:65535 --dport 53 -m state --state NEW,ESTABLISHED
_tcp_input_accept --sport 53 -d $SERVER_IP --dport 1024:65535 -m state --state ESTABLISHED
_s

:<<\_s
# establish a TCP chain and a UDP chain that we add all further rules to
# separate tables permit us to append rules to them while having order maintained within the default table
# iow: we don't run into the problem of appending a rule after a rule that rejects its traffic
sudo iptables -N PICASSO_INPUT
sudo iptables -N PICASSO_OUTPUT

#sudo iptables -A INPUT -p udp -j PICASSO_INPUT -m comment --comment "$_TOP_:$LINENO"
#sudo iptables -A INPUT -p tcp -j PICASSO_INPUT -m comment --comment "$_TOP_:$LINENO"
#sudo iptables -A OUTPUT -p udp -j PICASSO_OUTPUT -m comment --comment "$_TOP_:$LINENO"
#sudo iptables -A OUTPUT -p tcp -j PICASSO_OUTPUT -m comment --comment "$_TOP_:$LINENO"
sudo iptables -A INPUT -j PICASSO_INPUT
sudo iptables -A OUTPUT -j PICASSO_OUTPUT
_s

# ----------
:<<\_s
# Allow Ping from Outside(LAN) to Inside
#sudo iptables -A INPUT -s 192.168.1.0/24 -p icmp --icmp-type echo-request -j ACCEPT
#sudo iptables -A OUTPUT -p icmp --icmp-type echo-reply -j ACCEPT
#sudo iptables -A INPUT -p icmp --icmp-type 8 -m conntrack --ctstate NEW -j ACCEPT
sudo iptables -A INPUT -p icmp --icmp-type 0 -m comment --comment "$_TOP_:$LINENO" -j ACCEPT
sudo iptables -A INPUT -p icmp --icmp-type 3 -m comment --comment "$_TOP_:$LINENO" -j ACCEPT
sudo iptables -A INPUT -p icmp --icmp-type 11 -m comment --comment "$_TOP_:$LINENO" -j ACCEPT

# echo "Allow outgoing icmp connections (pings,...)"
#sudo iptables -A OUTPUT -p icmp --icmp-type echo-request -j ACCEPT
sudo iptables -A OUTPUT -p icmp -m conntrack --ctstate NEW,ESTABLISHED,RELATED -m comment --comment "$_TOP_:$LINENO" -j ACCEPT
sudo iptables -A INPUT  -p icmp -m conntrack --ctstate ESTABLISHED,RELATED -m comment --comment "$_TOP_:$LINENO" -j ACCEPT
#sudo iptables -A INPUT -p icmp --icmp-type echo-reply -j ACCEPT

## Enable or allow incoming ICMP ping requests
#sudo iptables -A INPUT -p icmp --icmp-type 8 -s 0/0 -d 0/0 -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT
#sudo iptables -A OUTPUT -p icmp --icmp-type 0 -s 0/0 -d 0/0 -m state --state ESTABLISHED,RELATED -j ACCEPT
## Allow or enable outgoing ping requests
#sudo iptables -A OUTPUT -p icmp --icmp-type 8 -s $IP -d 0/0 -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT
#sudo iptables -A INPUT -p icmp --icmp-type 0 -s 0/0 -d $IP -m state --state ESTABLISHED,RELATED -j ACCEPT

# we will permit ping, but rate-limit type 8 to prevent DoS-attack
sudo iptables -A INPUT -p icmp --icmp-type 8 -m limit --limit 1/second -m comment --comment "$_TOP_:$LINENO" -j ACCEPT
_s

# echo "Allow incoming icmp connections (pings,...)"
#sudo iptables -A INPUT -p icmp --icmp-type 8 -s 0/0 -d 0/0 -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT
#sudo iptables -A OUTPUT -p icmp --icmp-type 0 -s 0/0 -d 0/0 -m state --state ESTABLISHED,RELATED -j ACCEPT
#sudo iptables -A OUTPUT -p icmp -j ACCEPT
#sudo iptables -A INPUT -p icmp -j ACCEPT

# echo-reply     :  0
# destination-unreachable : 3
# redirect        : 5
# echo-request   :  8
# time-exceeded  : 11
:<<\_x
iptables -p icmp -h  # list ping types
_x

# Allow Ping from Outside to Inside
#sudo iptables -A OUTPUT -p icmp --icmp-type echo-request -s 0/0 -m limit --limit 10/s -j ACCEPT -m comment --comment "echo-request"
#sudo iptables -A INPUT -p icmp --icmp-type echo-reply -j ACCEPT

sudo iptables -A INPUT -p icmp --icmp-type echo-request -s 0/0 -m state --state NEW,ESTABLISHED,RELATED -m limit --limit 10/s -j ACCEPT -m comment --comment "echo-request"
sudo iptables -A OUTPUT -p icmp --icmp-type echo-reply -d 0/0 -m state --state ESTABLISHED,RELATED -j ACCEPT

#iptables -A INPUT -p icmp --icmp-type 8 -s 0/0 -d $MNIC_IP -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT
#iptables -A OUTPUT -p icmp --icmp-type 0 -s $MNIC_IP -d 0/0 -m state --state ESTABLISHED,RELATED -j ACCEPT
#iptables -A INPUT -p icmp --icmp-type 8 -s 0/0 -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT
#iptables -A OUTPUT -p icmp --icmp-type 0 -d 0/0 -m state --state ESTABLISHED,RELATED -j ACCEPT

#iptables -A INPUT -p icmp --icmp-type destination-unreachable -s 0/0 -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT


# Allow Ping from Inside to Outside
#sudo iptables -A INPUT -p icmp --icmp-type echo-request -j ACCEPT -m state --state NEW,ESTABLISHED,RELATED -m comment --comment "echo-request"
#sudo iptables -A OUTPUT -p icmp --icmp-type echo-reply -d 0/0 -m state --state ESTABLISHED,RELATED -m limit --limit 10/s -j ACCEPT -m comment --comment "echo-reply"

sudo iptables -A OUTPUT -p icmp --icmp-type echo-request -d 0/0 -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT -m comment --comment "echo-request"
sudo iptables -A INPUT -p icmp --icmp-type echo-reply -s 0/0 -m state --state ESTABLISHED,RELATED -m limit --limit 10/s -j ACCEPT -m comment --comment "echo-reply"

sudo iptables -A INPUT -p icmp --icmp-type destination-unreachable -d 0/0 -m limit --limit 10/s -j ACCEPT -m comment --comment "destination-unreachable"
sudo iptables -A INPUT -p icmp --icmp-type redirect -d 0/0 -m limit --limit 10/s -j ACCEPT -m comment --comment "redirect"
sudo iptables -A INPUT -p icmp --icmp-type time-exceeded -s 0/0 -j ACCEPT -m comment --comment "time-exceeded"

:<<\_s
# ICMP
# We accept icmp in if it is "related" to other connections (e.g a time exceeded (11)
# from a traceroute) or it is part of an "established" connection (e.g. an echo reply (0)
# from an echo-request (8)).
sudo iptables -A INPUT -p icmp -i $IFACE -m state --state ESTABLISHED,RELATED -m comment --comment "$_TOP_:$LINENO" -j ACCEPT
# We always allow icmp out.
sudo iptables -A OUTPUT -p icmp -o $IFACE -m state --state NEW,ESTABLISHED,RELATED -m comment --comment "$_TOP_:$LINENO" -j ACCEPT
_s

## TRACEROUTE
# Outgoing traceroute anywhere.
# The reply to a traceroute is an icmp time-exceeded which is dealt with by the next rule.
#iptables -A OUTPUT -p udp -o $IFACE -m state --state NEW -m comment --comment "$_TOP_:$LINENO" -j ACCEPT

:<<\_s
sudo iptables -A INPUT -p udp -j REJECT --reject-with icmp-port-unreachable
sudo iptables -A INPUT -p tcp -j REJECT --reject-with tcp-rst

sudo iptables -A INPUT -j REJECT --reject-with icmp-cat-unreachable
_s
:<<\_x
sudo tcpdump -i any -n -v 'icmp[icmptype] = icmp-echoreply or icmp[icmptype] = icmp-echo'
_x


# ----------
:<<\_c
# DHCP client
# http://www.linklogger.com/UDP67_68.htm
client request: UDP 0.0.0.0:68 -> 255.255.255.255:67
server reply: UDP 192.168.1.20:67 -> 255.255.255.255:68
client renewal request: UDP 192.168.1.20:67 -> 192.168.1.1:68
server renewal reply: UDP 192.168.1.1:68 -> 192.168.1.20:67
_c

_debug_firewall sf0s033ljfs0

if [[ -n "$IFACE" ]]; then

_debug_firewall "IFACE: $IFACE"

# SSH - from a remote client into the vm via vagrant
# Allow incoming SSH connection from vagrant(10.0.2.2) -> $IFACE(tcp/22)
_tcp_input_accept -i $IFACE --dport 22 -s 10.0.2.2 -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "vagrant-ssh-server"
_tcp_output_accept -o $IFACE --sport 22 -d 10.0.2.2 -m conntrack --ctstate ESTABLISHED -m comment --comment "vagrant-ssh-server"

#sudo iptables -A INPUT -i $IFACE -p tcp -s $GW -m multiport --dports 67,68 -m comment --comment "$_TOP_:$LINENO" -j ACCEPT
#sudo iptables -A INPUT -i $IFACE -p udp -s $GW -m multiport --dports 67,68 -m comment --comment "$_TOP_:$LINENO" -j ACCEPT

#_tcp_input_accept -i $IFACE -s 10.0.2.15 -m multiport --dports 67,68  # vagrant
#_tcp_output_accept -o $IFACE -d 10.0.2.15 -m multiport --dports 67,68  # vagrant

# DHCP client
_udp_output_accept -o $IFACE -d 10.0.2.2 -m multiport --dports 67,68 -m comment --comment "vagrant-dhcp-client"
_udp_input_accept -i $IFACE -s 10.0.2.2 -m multiport --sports 67,68 -m comment --comment "vagrant-dhcp-client"
fi

_debug_firewall 935739urowfw

if [[ -n "$trusted_iface" ]]; then

_debug_firewall "trusted_iface: $trusted_iface, GW: $GW"

# SSH in from LAN(192.168.1.0/24) -> $trusted_iface(tcp/22)
#_tcp_input_accept --dport 22 -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "ssh"
#_tcp_output_accept --sport 22 -m conntrack --ctstate ESTABLISHED -m comment --comment "ssh"
_tcp_input_accept -i $trusted_iface -s $trusted_cidr --dport 22 -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "ssh-server"
_tcp_output_accept -o $trusted_iface -d $trusted_cidr --sport 22 -m conntrack --ctstate ESTABLISHED -m comment --comment "ssh-server"

:<<\_s
# SSH out to $trusted_iface/$trusted_cidr(tcp/22)
# Allow outgoing SSH connection: $trusted_iface(tcp/22) <-> LAN(192.168.1.0/24)
_tcp_output_accept -o $trusted_iface -d $trusted_cidr --dport 22 -m conntrack --ctstate NEW,ESTABLISHED
_tcp_input_accept -i $trusted_iface -s $trusted_cidr --sport 22 -m conntrack --ctstate ESTABLISHED
_s

# DHCP/BOOTP client
_udp_output_accept -o $trusted_iface -m multiport --dports 67,68 -m comment --comment "dhcp-client"
_udp_input_accept -i $trusted_iface -m multiport --sports 67,68 -m comment --comment "dhcp-client"
_tcp_output_accept -o $trusted_iface -m multiport --dports 67,68 -m comment --comment "dhcp-client"
_tcp_input_accept -i $trusted_iface -m multiport --sports 67,68 -m comment --comment "dhcp-client"
fi

_debug_firewall jsfs57s6f9sfiwh

# ----------
for ip in $PACKAGE_SERVER; do

_debug_firewall "Allow outgoing connection to '$ip' for ftp, http, https"
_tcp_output_accept -d $ip -m multiport --dports 80,443 -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "http/https-client"
_tcp_input_accept -s $ip -m multiport --sports 80,443 -m conntrack --ctstate ESTABLISHED -m comment --comment "http/https-client"
done

_debug_firewall f93274wfos0fsd

# ----------
# NTP client
_udp_output_accept --dport 123 -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "ntp-client"
_udp_input_accept --sport 123 -m conntrack --ctstate ESTABLISHED -m comment --comment "ntp-client"


# ----------
# ignore Pre-Windows 2000 systems NetBIOS over TCP/IP (ports 137,138,139)
_udp_input_drop -m multiport --dports 137:138,515 -m comment --comment "netbios"
#_tcp_input_drop -m multiport --dports 139,445,515 -m comment --comment "netbios"
_udp_output_drop -m multiport --sports 137:138,515 -m comment --comment "netbios"
#_tcp_output_drop -m multiport --sports 139,445,515 -m comment --comment "netbios"

# ignore syncthing
_udp_input_drop --dport 21027 -m comment --comment "syncthing"
_tcp_input_drop --dport 22000 -m comment --comment "syncthing"

[[ ${TEST^^} == 'TRUE' ]] && {
# the network may not be operative
ping google.com -c1 1>/dev/null  # test ICMP and DNS
}

:<<\_c
establish a TCP chain and a UDP chain that we add all further rules to...

standard filtering rules have all been applied
we now add custom tables where we direct all packets that fall through the standard rules
our additional rules should be added to these tables
that way we can purge the tables without affecting the standard rules
and we can attach logging that does not output the standard rules

# separate tables permit us to append rules to them while having order maintained within the default table
# iow: we don't run into the problem of appending a rule after a rule that rejects its traffic
_c

_debug_firewall a

sudo iptables -N PICASSO_INPUT
sudo iptables -N PICASSO_OUTPUT

_debug_firewall b

sudo iptables -A INPUT -p udp -j PICASSO_INPUT
sudo iptables -A INPUT -p tcp -j PICASSO_INPUT

_debug_firewall c

sudo iptables -A OUTPUT -p udp -j PICASSO_OUTPUT
sudo iptables -A OUTPUT -p tcp -j PICASSO_OUTPUT

_debug_firewall d

#sudo iptables -A INPUT -m conntrack --ctstate INVALID -j DROP

:<<\_x
sudo iptables -L PICASSO_INPUT
_x
}


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
function input-accept-forward-accept-output-accept() {

_debug_firewall "_iptables://input-accept-forward-accept-output-accept @: $@"

# i let everything through here - bad
#IPT="iptables"
sudo iptables -F
sudo iptables -X
sudo iptables -t nat -F
sudo iptables -t nat -X
sudo iptables -t mangle -F
sudo iptables -t mangle -X
sudo iptables -P INPUT ACCEPT
sudo iptables -P FORWARD ACCEPT  # Allow Internal Network to External network
sudo iptables -P OUTPUT ACCEPT

_debug_firewall sf9s9sd9fsf
_build_default_chain $@ || _return "_build_default_chain $@"

_debug_firewall sfshjlsdf

$LOG || [[ -v DEBUG && $DEBUG -ge 2 ]] && {
# log what falls through
sudo iptables -A INPUT -j LOG -m limit --limit 12/min --log-level 4 --log-prefix 'IPT INPUT ACCEPT: '
sudo iptables -A FORWARD -j LOG -m limit --limit 12/min --log-level 4 --log-prefix 'IPT FORWARD ACCEPT: '
sudo iptables -A OUTPUT -j LOG -m limit --limit 12/min --log-level 4 --log-prefix 'IPT OUTPUT ACCEPT: '
:<<\_x
cat /var/log/iptables.log
_x
}

}


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
function input-drop-forward-drop-output-accept() {

_debug_firewall "_iptables://input-drop-forward-drop-output-accept @: $@"

sudo iptables -F
sudo iptables -X
sudo iptables -t nat -F
sudo iptables -t nat -X
sudo iptables -t mangle -F
sudo iptables -t mangle -X
sudo iptables -P INPUT DROP
sudo iptables -P FORWARD DROP
sudo iptables -P OUTPUT ACCEPT

_build_default_chain $@ || _return "_build_default_chain $@"

$LOG || [[ -v DEBUG && $DEBUG -ge 2 ]] && {
# log what falls through
echo w
sudo iptables -A INPUT -j LOG -m limit --limit 12/min --log-level 4 --log-prefix 'IPT INPUT DROP: '
echo x
sudo iptables -A FORWARD -j LOG -m limit --limit 12/min --log-level 4 --log-prefix 'IPT FORWARD DROP: '
echo y
sudo iptables -A OUTPUT -j LOG -m limit --limit 12/min --log-level 4 --log-prefix 'IPT OUTPUT ACCEPT: '
echo z
}

:<<\_s
# Log before dropping
sudo iptables -A INPUT -j LOG -m limit --limit 12/min --log-level 4 --log-prefix 'IP INPUT drop: '
#sudo iptables -A INPUT  -j DROP
 
#sudo iptables -A OUTPUT -j ACCEPT
_s

# i'm not sure what this is...
#sudo iptables -D INPUT -m conntrack --ctstate INVALID -j DROP

:<<\_s
# these are redundant with their policy already set to drop
# also, they may cause problems
# it is common practice to append rules to iptables
# if these rules exist then anything appended after them will be ignored
iptables -A INPUT -j DROP
iptables -A FORWARD -j DROP
_s

_debug_firewall "_iptables://input-drop-forward-drop-output-accept fini"

}


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
:<<\_c
configure iptables to drop all traffic except ssh for vagrant
http://www.thegeekstuff.com/2011/03/iptables-inbound-and-outbound-rules/
_c

function input-drop-forward-drop-output-drop() {

_debug_firewall "_iptables://input-drop-forward-drop-output-drop @: $@"

sudo iptables -F
sudo iptables -X
sudo iptables -t nat -F
sudo iptables -t nat -X
sudo iptables -t mangle -F
sudo iptables -t mangle -X
sudo iptables -P INPUT DROP
sudo iptables -P OUTPUT DROP
sudo iptables -P FORWARD DROP

_debug_firewall

_build_default_chain $@ || _return "_build_default_chain $@"

$LOG || [[ -v DEBUG && $DEBUG -ge 2 ]] && {
# log what falls through
sudo iptables -A INPUT -j LOG -m limit --limit 12/min --log-level 4 --log-prefix 'IPT INPUT DROP: '
sudo iptables -A FORWARD -j LOG -m limit --limit 12/min --log-level 4 --log-prefix 'IPT FORWARD DROP: '
sudo iptables -A OUTPUT -j LOG -m limit --limit 12/min --log-level 4 --log-prefix 'IPT OUTPUT DROP: '
}

:<<\_s
# Log before dropping
sudo iptables -A INPUT -j LOG -m limit --limit 12/min --log-level 4 --log-prefix 'IP INPUT drop: '
#sudo iptables -A INPUT  -j DROP
 
sudo iptables -A OUTPUT -j LOG -m limit --limit 12/min --log-level 4 --log-prefix 'IP OUTPUT drop: '
#sudo iptables -A OUTPUT -j DROP

iptables -L
_s

:<<\_s
# these are redundant with their policy already set to drop
# also, they may cause problems
# it is common practice to append rules to iptables
# if these rules exist then anything appended after them will be ignored
iptables -A INPUT -j DROP  
iptables -A OUTPUT -j DROP  
iptables -A FORWARD -j DROP
_s

}


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
function _user_chain_flush() {

sudo iptables -F PICASSO_INPUT
sudo iptables -F PICASSO_OUTPUT
sudo iptables -F PICASSO_INPUT
sudo iptables -F PICASSO_OUTPUT
}


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
function _user_chain_log() {

sudo iptables -I PICASSO_INPUT -j LOG --log-level 4 --log-prefix 'IPT PICASSO_INPUT start: '  # prepend
sudo iptables -A PICASSO_INPUT -j LOG --log-level 4 --log-prefix 'IPT PICASSO_INPUT done: '  # append
sudo iptables -I PICASSO_OUTPUT -j LOG --log-level 4 --log-prefix 'IPT PICASSO_OUTPUT start: '  # prepend
sudo iptables -A PICASSO_OUTPUT -j LOG --log-level 4 --log-prefix 'IPT PICASSO_OUTPUT done: '  # append
}

function _user_chain_stop_log() {

sudo iptables -D PICASSO_INPUT -j LOG --log-level 4 --log-prefix 'IPT PICASSO_INPUT start: '  # delete
sudo iptables -D PICASSO_INPUT -j LOG --log-level 4 --log-prefix 'IPT PICASSO_INPUT done: '  # delete
sudo iptables -D PICASSO_OUTPUT -j LOG --log-level 4 --log-prefix 'IPT PICASSO_OUTPUT start: '  # delete
sudo iptables -D PICASSO_OUTPUT -j LOG --log-level 4 --log-prefix 'IPT PICASSO_OUTPUT done: '  # delete
}

# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
:<<\_c
# test user-defined chain method and various rules

these tests work by 
a) establish a full drop policy
b.1) flush the user-defined chains
b.2) enabling only what is necessary
b.3) run the test
b.4) repeat for each test
_c

function _test() {

declare -n _nic=${IFACES[MNIC]}
# iface overload
#trusted_iface=${_nic[iface]}
trusted_iface=${_nic[ifacekey],,}
trusted_cidr=${_nic[cidr]}
:<<\_x
trusted_iface=enp0s8
trusted_cidr=192.168.1.0/24
_x

_debug_firewall "trusted_iface=$trusted_iface trusted_cidr=$trusted_cidr"

# establish a full drop policy
. iptables.fun

input-drop-forward-drop-output-drop $trusted_iface $trusted_cidr

# ----------
_debug_firewall "cifs client test"

_user_chain_flush

# 445/TCP - SMB - Microsoft Directory Services (microsoft-ds) - Windows File and Print Sharing - Windows 2000 and XP - only for trusted networks 
# to turn off in Windows disable the NetBIOS over TCP/IP driver (NetBT.sys).
# https://www.grc.com/port_445.htm
_tcp_in_accept -i $trusted_iface -s $trusted_cidr --dport 445 -m state --state NEW,ESTABLISHED,RELATED
_tcp_out_accept -o $trusted_iface -d $trusted_cidr --sport 445 -m state --state ESTABLISHED,RELATED

_debug_firewall "[OK] cifs client test"

# ----------
# http/https client

sudo iptables -A PICASSO_OUTPUT -p tcp -m multiport --dports 80,443 -j ACCEPT
#curl $GW  # no DNS
curl google.com  # yes DNS
}


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
function _troubleshoot() {

declare -n _nic=${IFACES[MNIC]}
# iface overload
#trusted_iface=${_nic[iface]}
trusted_iface=${_nic[ifacekey],,}
trusted_cidr=${_nic[cidr]}
:<<\_x
trusted_iface=enp0s8
trusted_cidr=192.168.1.0/24
_x


# full accept just so we can use cifs to access this script file - samba must be operative to access $PR/$PV/install
IPT="iptables"
sudo iptables -F
sudo iptables -X
sudo iptables -t nat -F
sudo iptables -t nat -X
sudo iptables -t mangle -F
sudo iptables -t mangle -X
sudo iptables -P INPUT ACCEPT
sudo iptables -P FORWARD ACCEPT
sudo iptables -P OUTPUT ACCEPT

. $PR/$PV/install/system/iptables/default

# establish a full drop policy
input-drop-forward-drop-output-drop

# ----------
:<<\_s
_iptables://input-drop-forward-drop-output-drop
sudo iptables -A OUTPUT -p tcp --dport 445 -m state --state NEW,ESTABLISHED -j ACCEPT
ls $PR/$PV/install
[FAIL] appending rule in default chain after its traffic has already been dropped
_s

_debug_firewall "trusted_iface: $trusted_iface, trusted_cidr: $trusted_cidr"

# ----------
# ssh from a remote host into the vm via vagrant
_user_chain_flush
# Allow incoming SSH connection: vagrant(10.0.2.2) <-> $IFACE(tcp/22)
_iptables "-A PICASSO_INPUT -p tcp -i $IFACE -s 10.0.2.2 --dport 22 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT"
_iptables "-A PICASSO_OUTPUT -p tcp -o $IFACE -d 10.0.2.2 --sport 22 -m conntrack --ctstate ESTABLISHED -j ACCEPT"
# Allow incoming SSH connection from any lan ip($trusted_cidr) <-> $IFACE(tcp/22)
if [[ -n "$trusted_iface" ]]; then
_iptables "-A PICASSO_INPUT -p tcp -i $trusted_iface -s $trusted_cidr --dport 22 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT"
_iptables "-A PICASSO_OUTPUT -p tcp -o $trusted_iface -d $trusted_cidr --sport 22 -m conntrack --ctstate ESTABLISHED -j ACCEPT"
fi

# ---------- ----------
sudo iptables -F PICASSO_INPUT
sudo iptables -F PICASSO_OUTPUT
sudo iptables -F PICASSO_INPUT
sudo iptables -F PICASSO_OUTPUT
sudo iptables -A PICASSO_INPUT -j ACCEPT
sudo iptables -A PICASSO_OUTPUT -j ACCEPT
sudo iptables -A PICASSO_INPUT -j ACCEPT
sudo iptables -A PICASSO_OUTPUT -j ACCEPT


# ---------- ----------


sudo iptables -A PICASSO_INPUT -p tcp --dport 135 -m state --state NEW,ESTABLISHED -j ACCEPT
sudo iptables -A PICASSO_OUTPUT -p tcp --sport 135 -m state --state ESTABLISHED -j ACCEPT

sudo iptables -A PICASSO_INPUT -p tcp --dport 593 -m state --state NEW,ESTABLISHED -j ACCEPT
sudo iptables -A PICASSO_OUTPUT -p tcp --sport 593 -m state --state ESTABLISHED -j ACCEPT


sudo iptables -A PICASSO_INPUT -p tcp --dport 135 -j ACCEPT
sudo iptables -A PICASSO_OUTPUT -p tcp --sport 135 -j ACCEPT
sudo iptables -A PICASSO_INPUT -p tcp --sport 135 -j ACCEPT
sudo iptables -A PICASSO_OUTPUT -p tcp --dport 135 -j ACCEPT

sudo iptables -A PICASSO_INPUT -p tcp --dport 593 -j ACCEPT
sudo iptables -A PICASSO_OUTPUT -p tcp --sport 593 -j ACCEPT


# http/https server
sudo iptables -A PICASSO_INPUT -p tcp -m multiport --dports 80,443 -j ACCEPT
}


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
_iptables_provision  # install iptables


# ----------
_debug_firewall "IFACES[MNIC]: ${IFACES[MNIC]}"

[[ -v IFACES[MNIC] ]] && {

declare -n _nic=${IFACES[MNIC]}
# iface overload
#trusted_iface=${_nic[iface]}
trusted_iface=${_nic[ifacekey],,}  # trusted_iface=MNIC - however, i use a variable in case ifacekey should be changed
trusted_cidr=${_nic[cidr]}

_debug_firewall "trusted_iface: $trusted_iface, trusted_cidr: $trusted_cidr"
}

[[ -n $1 ]] && {

_debug_firewall "$1 $trusted_iface $trusted_cidr"

#_${1} $trusted_iface $trusted_cidr  # configure iptables

_url_parse $1 FW

_debug_firewall "$(declare -p FW)"

_debug_firewall "${FW[hostname]} $trusted_iface $trusted_cidr"

${FW[hostname]} $trusted_iface $trusted_cidr || {
echo sowssfw972
_return "${FW[hostname]} $trusted_iface $trusted_cidr"  # configure nftables
}

}

_debug_firewall "e"

iptables -N TEMP_IN
iptables -N TEMP_OUT
#iptables -N TEMP_FORWARD
iptables -I INPUT -j TEMP_IN
iptables -I OUTPUT -j TEMP_OUT
#iptables -I FORWARD -j TEMP_FORWARD

_debug_firewall "f"

#iptables -A INPUT -p tcp --dport 22 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT
#iptables -A OUTPUT -p tcp --sport 22 -m conntrack --ctstate ESTABLISHED -j ACCEPT

# permit client access to Windows' file shares
# this lingers after provisioning
[[ -n "${IFACES[MNIC]}" ]] && {

declare -n _nic=${IFACES[MNIC]}

_debug_firewall "9sf7trlsf09sf _nic{[@]: ${_nic[@]}"

# this is added to the custom chains so it is easy to remove later
iptables -A TEMP_OUT -j ACCEPT -o ${_nic[iface]} -d ${_nic[cidr]} -p tcp -m multiport --dports 445 -m conntrack --ctstate NEW,ESTABLISHED
iptables -A TEMP_IN -j ACCEPT -i ${_nic[iface]} -s ${_nic[cidr]} -p tcp -m multiport --sports 445 -m conntrack --ctstate ESTABLISHED
}

_debug_firewall "g"

iptables -A TEMP_OUT -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT
#iptables -A TEMP_FORWARD -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT
#iptables -A TEMP_IN -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT
iptables -A TEMP_IN -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT

_debug_firewall "e"

:<<\_c
9418: git
Git protocol requires access to port 9418, which is not a standard port that corporate firewalls always allow. If you are behind a firewall or on a proxy that blocks Git protocol, modify the enable_plugin line to use https:// instead of git:// and add GIT_BASE=https://git.openstack.org to the credentials:
_c
_debug_firewall wsosofww
GIT_PORT=9418
iptables -A TEMP_OUT -j ACCEPT -p tcp -m multiport --dports $GIT_PORT -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment 'git-client'
iptables -A TEMP_IN -j ACCEPT -p tcp -m multiport --sports $GIT_PORT -m conntrack --ctstate ESTABLISHED -m comment --comment 'git-client'
:<<\_x
sudo lsof -i:9418
netstat -ntpl | grep -i 9418
_x

:<<\_x
sudo iptables -L TEMP_IN  # list
sudo iptables -F TEMP_IN  # flush
sudo iptables -X TEMP_IN  # then delete
sudo iptables-save > /vagrant/iptables
_x


# ----------
_debug_firewall "iptables done"
:<<\_c
standard syslog levels...
log-level 0 - emergency
log-level 4 - warning
log-level 7 - debug
_c
:<<\_x
logging the INPUT and OUTPUT chains is too verbose, because it catches the ssh packets and everything else
sudo iptables -I INPUT -j LOG --log-prefix 'IPT INPUT log all: '
sudo iptables -I OUTPUT -j LOG --log-prefix 'IPT OUTPUT log all: '
sudo iptables -D INPUT -j LOG --log-prefix 'IPT INPUT log all: '
sudo iptables -D OUTPUT -j LOG --log-prefix 'IPT OUTPUT log all: '
_x
:<<\_x
the position of the PICASSO_INPUT and PICASSO_OUTPUT links, in their respecitve INPUT and OUTPUT chains, means that standard protocol packets have already been filtered out before they are reached
thus, PICASSO_INPUT and PICASSO_OUTPUT are effective points to trace from

add these rules after provisioning is complete

. iptables-install.sh
_user_chain_log
sudo journalctl -f | grep IPT

# anything falling through is an indication that the PICASSO_INPUT/PICASSO_OUTPUT rules are not complete

_user_chain_stop_log
_x
:<<\_x
sudo journalctl -f | grep SRC=192.168.1.79

iptables -A INPUT -j LOG -m limit --limit 12/min --log-level 4 --log-prefix 'IPT INPUT DROP: '
iptables -A FORWARD -j LOG -m limit --limit 12/min --log-level 4 --log-prefix 'IPT FORWARD DROP: '
iptables -A OUTPUT -j LOG -m limit --limit 12/min --log-level 4 --log-prefix 'IPT OUTPUT ACCEPT: '
_x
:<<\_x
controller
_open_server_firewall MNIC 80 test 'TCP' "-m limit --limit 25/minute --limit-burst 100"
_x

_debug_firewall "iptables -Z"

iptables -Z  # Zero the packet and byte counters in all chains.

true
