:<<\_c
. $PR/$PV/guest/bin/config.fun

see: $PROOF/picasso/$PV/guest/bin/config.cookbook
_c

:<<\_c
_prepend_file_before_line appends injections such that you can inject more than one and they will execute in sequence
_insert_file_between_lines copies injections over any existing content such that the provisioner can hold default script that may be overwritten by bespoke script
_c


# ---------- ----------
function _insert_text_over_lines() {  # <start> <stop> <dfile> <text>

#echo "_insert_text_over_lines @: $@"

1>/dev/null ex $3 <<< "g/$2/
i
${4:-$(</dev/stdin)}
.
wq
"

ex -s -c "/^$1/+0,/^$2/-0 d" -c "wq" "$3"  # delete enclosing lines
}


# ---------- ----------
function _insert_text_between_lines() {  # <start> <stop> <dfile> <text>

#echo "_insert_text_between_lines @: $@"

1>/dev/null ex $3 <<< "g/$2/
i
${4:-$(</dev/stdin)}
.
wq
"
}


# ---------- ----------
function _insert_file_over_lines() {

ex -s -c "/^$1/+0 r $4" -c "/^$1/+0,/^$2/-0 d" -c "wq" "$3"
}


# ---------- ----------
function _insert_file_between_lines() {
#echo ex -s -c "\"/^$1/+1,/^$2/-1 d\" -c \"/^$1/ r $4\" -c \"wq\" $3"
ex -s -c "/^$1/+1,/^$2/-1 d" -c "/^$1/ r $4" -c "wq" "$3"
}
:<<\_x
ex -s -c "/^#PRE_PROVISION_START/+1,/^#PRE_PROVISION_STOP/-1 d" \
  -c "/^#PRE_PROVISION_START/ r ./provisioner.sh.pre" \
  -c "wq" ./provisioner.sh

filename=provisioner.sh
bname=provisioner.sh
_insert_file_between_lines '#PRE_PROVISION_START' '#PRE_PROVISION_STOP' "./$filename" ${bname}.pre
_x


# ---------- ----------
:<<\_c
s="Hello, World!"

_append_text_after_line $start $infile "$s"
_c

function _append_text_after_line() {  # <line> <dfile> <text>

sed -i "/$1/a \
$3
" "$2"
}


# ---------- ----------
:<<\_c
s="Hello, World!"

_append_text_over_line "$start" $infile "$s"
_c

function _append_text_over_line() {  # <line> <dfile> <text>

sed -i "/$1/c \
$3
" "$2"
}


# ---------- ----------
:<<\_c
_prepend_text_before_line "$stop" $dfile "<text>"
_c

function _prepend_text_before_line() {  # <line> <file> <text>

#echo "_prepend_text_before_line @: $@"

1>/dev/null ex $2 <<< "g/$1/
i
${3:-$(</dev/stdin)}
.
wq
"
}

:<<\_x
# you cannot pipe to a function
# that means an argument must be used
# however; using an argument prohibits the use of inner quotes

cat > try.txt <<!
#POST_PROVISION_START
#POST_PROVISION_STOP
!

function _prepend_text_before_line() {

declare s=${3:-$(</dev/stdin)}

ex $2 <<< "g/$1/
i
$s
.
wq
"
}

_prepend_text_before_line "#POST_PROVISION_STOP" try.txt "foobar"  # argument

_prepend_text_before_line "#POST_PROVISION_STOP" try.txt <<!  # piped
this "is" some text
!

cat try.txt
_x

:<<\_s
# this fails if the matching string is the last line in the file
sed -i "/$1/i \
$2
" $3
_s


# ---------- ----------
function _append_file_after_line() {  # <line> <dfile> <sfile>

#echo "_append_file_after_line @: $@"

1>/dev/null ex $2 <<< "g/$1/+1
i
$(<$3)
.
wq
"
}

:<<\_c
#sed -i -e "/$1/r '$3'" "$2"
sed  -e "/$stop/r $injection" $infile
_c

:<<\_x
cat > try.txt <<!
#POST_PROVISION_START
#POST_PROVISION_STOP
!

cat > injection.txt <<!
foo
"bar"
!

_append_file_after_line "#POST_PROVISION_START" try.txt injection.txt

cat try.txt
_x


# ---------- ----------
:<<\_c
if the target file does not end with a newline, then 'ed' appends one and prints to stderr the message 'Newline appended'
to reduce output noise, we use '2>/dev/null' before 'ed'
_c
:<<\_c
https://unix.stackexchange.com/questions/32908/how-to-insert-the-content-of-a-file-into-another-file-before-a-pattern-marker
_c
:<<\_c
$1 - tag
$2 - file
$3 - injection
_c

function _prepend_file_before_line() {  # <line> <dfile> <sfile>

#echo "_prepend_file_before_line @: $@"

1>/dev/null ex $2 <<< "g/$1/
i
$(<$3)
.
wq
"
}
