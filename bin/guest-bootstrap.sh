#!/usr/bin/env bash
[[ -v DEBUG && $DEBUG -ge 0 ]] && printf "\e[1;38;5;228m>>> %s @: %s\e[0m\n" "${BASH_SOURCE[0]}" "${*@Q}"

:<<\_c
$PR/$PV/bin/guest-bootstrap.sh

the first time guest-provisioner.sh is sourced, this script runs
only the first provisioner processes this...
chained and subsequent provisioners never get here

see: $PROOF/picasso/guest-bootstrap/
see: $PROOF/picasso/$PV/guest/bootstrap.md
_c

#echo "guest-bootstrap> *: $* - USER: $USER"
#DEBUG=2

ARG0=$0  # preserve for re-entry
OPTARGS=$*  # preserve for re-entry



# ----------
:<<\_c
we need to force PROVISIONER_USER=root to avoid re-entry and allow this script to complete...
PROVISIONER_USER=root . $PR/bin/guest-provisioner.sh

however; we have this built-in design...
000-network.sh -> $PR/bin/provider.env -> $PROVISIONER_USER

guest-provisioner.sh {
$PR/$PV/bin/guest-bootstrap.sh -> ~/.picasso/bin/020-provisioner.env -> $PROVISIONER_USER
}

therefore, we need to source guest-provisioner.sh before creating either of those files
_c

:<<\_c
NB: this file is incorporated into each basebox - iow: changes to this file require a rebuild of the basebox

this script receives arguments that Vagrant passes to it that originate within the Vagrantfile
in addition, Vagrant also sets the environment variable PROVISIONER=<name of current provisioning script file>
note that arguments are received as pairs (--<name>=[value])

[assumptions]
provisioner-env) may require vboxsf

[usage]
. guest-environment.sh
...
_c

. $PR/bin/provider.env  # -> $PASS_FILE_SRC, $PASS_FILE_DEST


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
:<<\_c
PROVISIONER_NAME is unique to each script and thus must be done for each one
PROVISIONER_USER may change and thus must be done for each one
_c

. longopts.sh  # <- $@(--*=*) -> longopts[], longvals[], $@

#echo "$(declare -p longopts)"
#echo "$(declare -p longvals)"
#echo "$(declare -p longargs)"
#echo "@: $@"

for i in ${!longopts[@]}; do
opt=${longopts[$i]}
val=${longvals[$i]}
case $opt in
provisioner-name) PROVISIONER_NAME=$val;;
provisioner-user) PROVISIONER_USER=$val;;
esac
done

#echo "guest-bootstrap> PROVISIONER_NAME: $PROVISIONER_NAME, PROVISIONER_USER: $PROVISIONER_USER, GUEST_PROVISIONER_ENV: $GUEST_PROVISIONER_ENV"


# ----------
#echo "bootstrap PROVISIONER_USER: $PROVISIONER_USER, whoami: $(whoami)"

[[ "$PROVISIONER_USER" != "$USER" ]] && {

#echo "guest-bootstrap> sudo -Hin -u $PROVISIONER_USER \"$ARG0\" $OPTARGS"

  sudo -Hin -u $PROVISIONER_USER "$ARG0" $OPTARGS; exit $?  # re-enter!
#  sudo -Hin -u $PROVISIONER_USER "$ARG0" "$OPTARGS"; exit $?  # re-enter!
}


# ----------
:<<\_c
at this point USER=$PROVISIONER_USER
_c

GUEST_PROVISIONER_ENV=~/.picasso/bin/020-provisioner.env  # autoload

#echo "guest-bootstrap> GUEST_PROVISIONER_ENV: $GUEST_PROVISIONER_ENV"

#echo "$(declare -p longopts)"
#echo "$(declare -p longvals)"

for i in ${!longopts[@]}; do
opt=${longopts[$i]}
val=${longvals[$i]}

case $opt in

provider-env)
:<<\_c
guest-bootstrap.sh bootstraps user provisioning
system provisioning must be performed before user provisioning
therefore, this has already been consumed by 000-network.sh
_c
;;

provisioner-env)

:<<\_c
provisioner.sh scripts have no knowledge that run-once bootstrap script preceded it
they simply have to follow convention of the bootstrap's results like the existence of ~/.picasso/bin/020-provisioner.env
_c

[[ -f $PR/bin/provisioner.env ]] || {

printf '\e[0;32mINFO: %s \e[0m\n' "Bootstrap loading provisioner-env from $val"  #] - _info not yet loaded

  if [[ "$val" =~ (^| )http:// ]]; then

#echo "guest-bootstrap> curl -s -o $PR/bin/provisioner.env $val"

    curl -s -o $PR/bin/provisioner.env $val

  elif [[ -f "$val" ]]; then

#echo "guest-bootstrap> cp \"$val\" $PR/bin/provisioner.env"

    cp "$val" $PR/bin/provisioner.env

  else
    _exit "File not found: $val"
  fi

  ln -s $PR/bin/provisioner.env $GUEST_PROVISIONER_ENV  # autoload

:<<\_s
cat >> $GUEST_PROVISIONER_ENV <<-!
  METADATA_SOURCE=$val
!
_s
}
;;

esac

done

:<<\_c
guest-environment.sh is also sourced at the end of this script via guest-provisioner.sh
_c

#echo "guest-bootstrap> . guest-environment.sh"

. guest-environment.sh  # <- $PR/$PV/bin/xxx-*.*, $PR/bin/xxx-*.*, ~/.picasso/bin/xxx-*.* -> longopts[], _debug()...


# ----------
[[ -n "$PASS_FILE_SRC" ]] && {

PASS_FILE_SRC=($PASS_FILE_SRC)  # string -> array
PASS_FILE_DEST=($PASS_FILE_DEST)  # string -> array

_debug_scp "$(declare -p PASS_FILE_SRC)"
_debug_scp "$(declare -p PASS_FILE_DEST)"

for ((i=0; i<${#PASS_FILE_SRC[@]}; i++)); do

:<<\_c
echo change '~' to "/home/$USER"

PASS_FILE_DEST='$HOME/foo/bar'  # single quotes
echo $(echo $PASS_FILE_DEST | HOME=/home/$USER envsubst)
_c
case $USER in

root)
#pass_file_dest=$(echo ${PASS_FILE_DEST[$i]} | HOME=/root FQDN=$(hostname -f) envsubst)
pass_file_dest=${PASS_FILE_DEST[$i]}
;;

*)

_debug_scp "guest-bootstrap> PASS_FILE_DEST[$i] ${PASS_FILE_DEST[$i]}"

if [[ ${PASS_FILE_DEST[$i]} == \~/* ]]; then

pass_file_dest="${PASS_FILE_DEST[$i]/#\~//home/$USER}"

#pass_file_dest=$(echo $pass_file_dest | HOME=/home/$USER FQDN=$(hostname -f) envsubst)

else
#pass_file_dest=$(echo ${PASS_FILE_DEST[$i]} | HOME=/home/$USER FQDN=$(hostname -f) envsubst)
pass_file_dest=${PASS_FILE_DEST[$i]}
fi

_debug_scp "guest-bootstrap> pass_file_dest: $pass_file_dest"

;;
esac

#set -a  # enable the export of every variable
#source $GUEST_PROVISIONER_ENV
#set +a  # disable

pass_file_dest=$(set -a && . $GUEST_PROVISIONER_ENV && echo $pass_file_dest | FQDN=$(hostname -f) envsubst)
#pass_file_dest=$(echo $pass_file_dest | FQDN=$(hostname -f) envsubst)

_debug_scp "guest-bootstrap> $USER pass_file_dest: $pass_file_dest"

[[ -d "$pass_file_dest" ]] || {

dir=$(dirname $pass_file_dest)

_debug_scp "guest-bootstrap> dir: $dir"

[[ -d $dir ]] || {

_debug_scp "guest-bootstrap> mkdir -p $dir"

:<<\_s
sudo mkdir -p $dir || _exit "sudo mkdir -p $dir"
[[ -n "$PROVISIONER_USER" ]] && sudo chown -R $PROVISIONER_USER:$PROVISIONER_USER $dir
_s

mkdir -p $dir || _exit "mkdir -p $dir"

#echo "guest-bootstrap> PASS_FILE_DEST[$i]: ${PASS_FILE_DEST[$i]}"

if [[ ${PASS_FILE_DEST[$i]} == \~/* ]]; then

#echo "guest-bootstrap> 2 PASS_FILE_DEST[$i]: ${PASS_FILE_DEST[$i]}"

# dir=~/dir1/dir2/dir3 - account for nested directories

:<<\_x
dir=~/app/foobar.picasso.digital:8000
echo "${dir#*/home/$USER}"  # "/app/foobar.picasso.digital:8000"

dir='test1:test2'
echo "${dir%%:*}"  # 'test1'
echo "${dir#*:}"  # 'test2'
_x

:<<\_c
if we were passed a dir/subdir/subdir/...
we want to chown from the first directory onward
_c

home_path=${dir#*/home/$USER}  # home_path=/app/foobar.picasso.digital:8000
first_dir=${home_path%${home_path#/*/}}  # first_dir=/app/

#echo "guest-bootstrap> home_path: $home_path, first_dir: $first_dir"

echo tryit2
#chown -R $USER:$USER $HOME/$first_dir
else

#echo "guest-bootstrap> 2 dir: $dir"

# dir=/tmp/dir1/dir2/dir3 - we cannot account for nested directories, because that would result in "chown /tmp"
echo tryit2
#chown $USER:$USER $dir
fi
}

_debug_scp "$(ls -la $dir)"

}

_debug_scp "$(ls -l $HOME)"

_debug_scp "guest-bootstrap> cp ${PASS_FILE_SRC[$i]} $pass_file_dest"

cp ${PASS_FILE_SRC[$i]} $pass_file_dest || _exit "cp ${PASS_FILE_SRC[$i]} $pass_file_dest"

echo tryit2
#chown $USER:$USER $pass_file_dest

done

}


# ----------
[[ -f "$GUEST_PROVISIONER_ENV" ]] || {

# provisioner.env was not specified as an argument so assume cloudinit functionality

LOCAL_METADATA_URL=http://169.254.169.253:8080/$HOSTNAME  # hard-coded

_debug3 "guest-bootstrap> assume cloudinit LOCAL_METADATA_URL: $LOCAL_METADATA_URL"

if curl -s -o $GUEST_PROVISIONER_ENV $LOCAL_METADATA_URL/provisioner.env; then

cat >> $GUEST_PROVISIONER_ENV <<!
METADATA_SOURCE=cloudinit
LOCAL_METADATA_URL=$LOCAL_METADATA_URL
!

else
_warn "Local metadata file not found: $LOCAL_METADATA_URL/provisioner.env"
fi
}


# ----------
_debug2 "guest-bootstrap> GUEST_PROVISIONER_ENV: $GUEST_PROVISIONER_ENV, METADATA_SOURCE: $METADATA_SOURCE, LOCAL_METADATA_URL: $LOCAL_METADATA_URL"

:<<\_c
~/.picasso/bin/020-provisioner.env is already loaded and it may declare $GUEST_PROVISIONER_ENV

the origin of GUEST_PROVISIONER_ENV may be cloudinit script or a virtualbox argument

NB: we append to any existing file
_c

:<<\_s
[[ -n "$GUEST_PROVISIONER_ENV" ]] && {

case "$METADATA_SOURCE" in

metafile)
_info "Loading metadata from /vagrant/$GUEST_PROVISIONER_ENV"
cat /vagrant/$GUEST_PROVISIONER_ENV >> $GUEST_PROVISIONER_ENV
;;

cloudinit)
_info "Loading metadata from $LOCAL_METADATA_URL/$GUEST_PROVISIONER_ENV"
curl -s $LOCAL_METADATA_URL/$GUEST_PROVISIONER_ENV >> $GUEST_PROVISIONER_ENV
;;

*) _error "Unknown METADATA_SOURCE: $METADATA_SOURCE";;

esac

_debug2 "$(ls -l $GUEST_PROVISIONER_ENV)"
_debug3 "$(cat $GUEST_PROVISIONER_ENV)"
}
_s


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
_debug2 "guest-bootstrap> READONLY_MNT: $READONLY_MNT, PICASSO_GIT_ENDPOINT: $PICASSO_GIT_ENDPOINT"

# if READONLY_MNT is defined, 011-mounts.sh will mount it

if [[ -n "$READONLY_MNT" ]]; then

_debug3 "$(ls -l $READONLY_MNT)"
_debug3 "$(ls -l $PR/v)"

:<<\_c
if the directory already exists, then it has already been initialized
it may already have been initialized within the basebox
or, it may already have been initialized by earlier chained provisioning

the question of whether or not to overwrite the existing directory arises
if we don't overwrite, then a new basebox must be used that contains the desired files?
if we do overwrite, then how do we do so only once and not by each chained provisioner?

philosophy...
that done within the basebox remains, and therefore, during basebox building, we don't initialize these directories
iow: $PR/core is pre-initialized within the basebox and does not get updated - use the basebox with the 'guest' files you need, because at provision time you cammot modify 'guest' files
other modules are installed on a first come, first served, basis
first come first served makes sense during provisioning, since the files should not change mid-provisioning
_c

[[ -d "$PR/$PV/instance" ]] || {
mkdir -p $PR/$PV/instance
cp -fr $READONLY_MNT/$PV/instance/* $PR/$PV/instance/
}

elif [[ -n "$PICASSO_GIT_ENDPOINT" ]]; then

:<<\_c
guest.git is statically cloned into each prebuilt basebox

$DESKSP/picasso/basebox/*/*/pre-provisioner.sh (

[[ -d "$PR/$PV/guest" ]] || { mkdir -p $PR/$PV/guest; curl -s $PICASSO_GIT_ENDPOINT/tar/raw/$PV/guest.tar.gz | tar -xz -C $PR/$PV/; }
}

instance.git is dynamically cloned into each provisioned instance
_c

[[ -d "$PR/$PV/instance" ]] || {

_debug "guest-bootstrap> git -c advice.detachedHead=false clone --branch $PV $PICASSO_GIT_ENDPOINT/instance.git $PR/$PV/instance"

if git -c advice.detachedHead=false clone --branch $PV $PICASSO_GIT_ENDPOINT/instance.git $PR/$PV/instance 2>&1; then

cp $PR/$PV/instance/bin/* $PR/$PV/bin/  # overlay bin/* to save having to add each repo's bin path to $PATH

else

_warn "FAIL: git -c advice.detachedHead=false clone --branch $PV $PICASSO_GIT_ENDPOINT/instance.git $PR/$PV/instance"

fi
}

:<<\_x
[[ -f "$PR/$PV/instance.txt" ]] || {
curl -s $PICASSO_GIT_ENDPOINT/tar/raw/$PV/instance.tar.gz | tar -xz -C $PR/$PV/
}
_x

else
_warn "Unknown PICASSO_GIT_ENDPOINT: $PICASSO_GIT_ENDPOINT"
fi

_debug3 "$(ls -la $PR/v)"


# ----------
declare -p PROVISIONER_SRC_ENDPOINTS &>/dev/null && {

. mount.fun  # -> _mount_smb(), _mount_nfs()

(( ${#PROVISIONER_SRC_ENDPOINTS[@]} > 0 )) && {

_debug "guest-bootstrap> $(declare -p PROVISIONER_SRC_ENDPOINTS)"

for (( i=0; i<${#PROVISIONER_SRC_ENDPOINTS[@]}; i++ )); do

_debug "guest-bootstrap> $(declare -p PROVISIONER_DEST_ENDPOINTS)"

_debug "guest-bootstrap> i0: $i"

_url_parse ${PROVISIONER_SRC_ENDPOINTS[$i]} SEP

:<<\_x
_url_parse ${PROVISIONER_DEST_ENDPOINTS[$i]} DEP
declare -p DEP
_x

DEST_ENDPOINT=${PROVISIONER_DEST_ENDPOINTS[$i]} 

_url_parse $DEST_ENDPOINT DEP

_debug "guest-bootstrap> i1: $i"

_debug "guest-bootstrap> $(declare -p SEP)"
_debug "guest-bootstrap> SEP[scheme]: ${SEP[scheme]}"

case ${SEP[scheme]} in

smb*)

_debug "guest-bootstrap> i2: $i"

# //192.168.1.6//readonly_smb

OPTIONS=${SEP[query]},uid=$(id -u $USER),gid=$(id -g $USER)  # ,noauto

_debug "guest-bootstrap> OPTIONS: $OPTIONS"

if UNC=//${SEP[host]}${SEP[path]} \
  OPTIONS=$OPTIONS \
  DEST_ENDPOINT=$DEST_ENDPOINT \
  _mount_smb; then

path=${DEP[path]#*$HOME/}  # strip "$HOME/" -> mnt/foo/bar
path=${path%%/*}  # -> mnt

:<<\_x
declare -A DEP
DEP[path]=/home/$USER/mnt/foo/bar
path=${DEP[path]#*$HOME/} 
echo "${path%%/*}"
_x

else
_error "UNC=//${SEP[host]}${SEP[path]} \
  OPTIONS=$OPTIONS \
  DEST_ENDPOINT=$DEST_ENDPOINT \
  _mount_smb"
fi

_debug "guest-bootstrap> i3: $i"

;;

nfs*)

OPTIONS=${SEP[query]}

_debug "guest-bootstrap> OPTIONS: $OPTIONS"

if UNC=//${SEP[host]}${SEP[path]} \
  OPTIONS=$OPTIONS \
  DEST_ENDPOINT=$DEST_ENDPOINT \
  _mount_nfs; then

path=${DEP[path]#*$HOME/}  # strip "$HOME/" -> mnt/foo/bar
path=${path%%/*}  # -> mnt

else
_error "UNC=//${SEP[host]}${SEP[path]} \
  OPTIONS=$OPTIONS \
  DEST_ENDPOINT=$DEST_ENDPOINT \
  _mount_nfs"
fi
;;

iscsi*)

OPTIONS=${SEP[query]}

_debug "guest-bootstrap> OPTIONS: $OPTIONS"

if UNC=//${SEP[host]}${SEP[path]} \
  OPTIONS=$OPTIONS \
  DEST_ENDPOINT=$DEST_ENDPOINT \
  _mount_iscsi; then

path=${DEP[path]#*$HOME/}  # strip "$HOME/" -> mnt/foo/bar
path=${path%%/*}  # -> mnt

#sudo chown -R $USER:$USER $HOME/$path

else
_error "UNC=//${SEP[host]}${SEP[path]} \
  OPTIONS=$OPTIONS \
  DEST_ENDPOINT=$DEST_ENDPOINT \
  _mount_iscsi"
fi
;;

esac

_debug "guest-bootstrap> i: $i"

done

}
}  # declare -p PROVISIONER_SRC_ENDPOINTS


# ----------
:<<\_c
PICASSO_LOG and /run/shm/picasso/log are dynamic. therefore configure their environment on the basis that they may be enabled at any time
_c

sudo mkdir -p /var/picasso

echo tryit2
sudo chown $USER:$USER /var/picasso
#sudo chown picasso:picasso /var/picasso  # don't use $USER, because initially it is 'root' which results in "permission denied" later on

:<<\_c
passing PICASSO_LOG_FILE through enables timestamping of the file so the host's log file syncs up with the guest's log file

local PICASSO_LOG_FILE+=".$(date +%T)"
_c

PICASSO_LOG_FILE=${PICASSO_LOG_FILE:-/var/picasso/picasso.log}

#[[ -f $PICASSO_LOG_FILE ]] || {
#sudo touch $PICASSO_LOG_FILE  # create or purge
touch $PICASSO_LOG_FILE  # create or purge
#sudo chown picasso:picasso $PICASSO_LOG_FILE
#}

# ----------
# this is deferred to last...

:<<\_c
guest-provisioner.sh is designed to re-enter itself if [[ $PROVISIONER_USER != $USER ]]
we do not re-enter this script due to the symlink change below 
the re-entry overlays the current shell with a new one and, most importantly, to terminate execution within the current shell
iow: if [[ $PROVISIONER_USER != $USER ]] this script will not execute anything below the sourcing of guest-provisioner.sh
the provisioner is the target to the re-entery
_c

:<<\_c
only run this script file once
$PR/bin/guest-provisioner.sh symlinked to $PR/$PV/bin/guest-provisioner_sh indicates the script has run
_c
ln -sf $PR/$PV/bin/guest-provisioner_sh $PR/bin/guest-provisioner.sh  # forced - to overwrite the symlink to this file

_debug3 "bootstrap5 @: $@"

. $PR/bin/guest-provisioner.sh $OPTARGS  # the new guest-provisioner_sh

_debug3 "bootstrap done"
