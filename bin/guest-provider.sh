[[ -v DEBUG && $DEBUG -ge 0 ]] && printf "\e[1;38;5;228m>>> %s @: %s\e[0m\n" "${BASH_SOURCE[0]}" "${*@Q}"

:<<\_c
guest-provider.sh

guest-provisioner.sh and guest-provisioner.sh are different in so far as pre-provisioners are passed different arguments than provisioners,
in addition, 'trigger' processes the provisioner argements and not the pre-provisioners arguments
therefore, firing trigger must be delayed until the first provisioner
iow: guest-provisioner.sh does not fire the trigger, while picasso-provisioner does file the trigger

see: $PROOF/picasso/$PV/guest/bootstrap.md
_c

#echo "guest-provider.sh @: $@"

. $PR/$PV/bin/longopts.sh #$@  # don't disturb current $@

#echo "qwwqb $(declare -p longopts)"
#echo "qwwqb $(declare -p longvals)"
#echo "qwwqb $(declare -p longargs)"
#echo qwwqb "@: $@"

PROVISIONER_NAME=unknown
PROVISIONER_USER=picasso  # hard-coded default in case it's not declared

for i in ${!longopts[@]}; do
opt=${longopts[$i]}
val=${longvals[$i]}
case $opt in
provisioner-name) PROVISIONER_NAME=$val;;
provisioner-user) PROVISIONER_USER=$val;;
provider-env)

[[ -f "$PR/bin/provider.env" ]] || {

#echo "Loading provider-env from $val"

  if [[ "$val" =~ (^| )http:// || "$val" =~ (^| )https:// ]]; then

#echo "curl -s -o $PR/bin/provider.env $val"

    curl -s -o $PR/bin/provider.env $val
#    curl -s -o $PR/bin/provider.env http://$HIP:8080/$val  # this insures we are getting a local file


cat >> $PR/bin/provider.env <<!
METADATA_SOURCE=cloudinit
#LOCAL_METADATA_URL=http://169.254.169.253:8080/$HOSTNAME
!

  elif [[ -f "$val" ]]; then

#echo "cp "$val" $PR/bin/provider.env"

    cp "$val" $PR/bin/provider.env

cat >> $PR/bin/provider.env <<!
METADATA_SOURCE=metafile
!

  else
    _exit "Environment file not found: $val"
  fi

chmod 644 $PR/bin/provider.env
}
;;
esac
done


# ----------
[[ -f "$PR/bin/provider.env" ]] || {

# provider.env was not specified as an argument so assume cloudinit functionality

echo "assume cloudinit"

curl -s -o $PR/bin/provider.env http://169.254.169.253:8080/$HOSTNAME/provider.env || {

printf "\e[1;41mERROR: %s \e[0m\n" "curl -s -o $PR/bin/provider.env http://169.254.169.253:8080/$HOSTNAME/provider.env"  #]
exit 1
}

chmod 644 $PR/bin/provider.env

cat >> $PR/bin/provider.env <<!
METADATA_SOURCE=cloudinit
LOCAL_METADATA_URL=http://169.254.169.253:8080/$HOSTNAME
!
}

cat >> $PR/bin/provider.env <<!
PROVISIONER_NAME=$PROVISIONER_NAME
PROVISIONER_USER=$PROVISIONER_USER
!


. $PR/bin/provider.env


# ----------
. guest-environment.sh || _return '. guest-environment.sh'

[[ -n "$PROVISIONER_NAME" ]] && _info "Running provisioner '$PROVISIONER_NAME' as '$USER'"


# ----------
true
