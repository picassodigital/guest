:<<\_c
. iptables.fun

MNIC and XNIC interfaces are handled differently
MNIC is assumed to be the management network - source and destination addresses are assumed to be $MNIC_CIDR
XNIC is assumed to be the external network - source and destination addresses are assumed to be 0.0.0.0/0
_c

function _debug_firewall() {
  [[ -v DEBUG_FIREWALL && $DEBUG_FIREWALL -ge 1 ]] && {
    local l=${#BASH_LINENO[@]}
    local f=${BASH_SOURCE[1]}
    f=$(basename ${f:-#})
    printf "\e[1;32m--->%b:%s %s\e[0m\n" "$f" "${BASH_LINENO[-$l]}" "$*"
  } || true
}

#echo hghgsho
#declare -p IFACES
#declare -p IFACE_MNIC


# ---------- ---------- ---------- ---------- ----------
:<<\_c
copies all rules to $PR/iptables and applies the rule ephemerally
using this indirect function, persistent iptables changes can then be made centrally by applying the rules accumulated in $PR/iptables
# $1:rule
_c

function _iptables() {

echo "$1" >> $PR/iptables

iptables $1
}


# ---------- ----------
:<<\_c
FROM=<any or ip or cidr> TO=<any or ip or cidr> _allow() {  # <iface> <port> <proto> [comment]
_c

function _allow_in() {  # --in=<iface> --from=<cidr> --out=<iface> --to=<cidr> --proto=<tcp|udp> <port> [comment]

local i IN FROM PROTO

_debug_firewall "iptables.fun:_allow_in> MODE=$MODE _allow_in $@"

. longopts.sh  # <- $@ -> longopts[], longvals[], longargs[], $@

for i in ${!longopts[@]}; do
opt=${longopts[$i]}
val=${longvals[$i]}

case $opt in
from)
if [[ "$val" = 'any' ]]; then
FROM=0.0.0.0/0
else
FROM=$val
fi
;;
in) IN=$val;;
out) OUT=$val;;
to)
if [[ "$val" = 'any' ]]; then
TO=0.0.0.0/0
else
TO=$val
fi
;;
proto) PROTO=$val;;
esac

done

if [[ "${MODE^^}" == 'CLIENT' ]]; then

_debug_firewall "sudo iptables -A ${INPUT_CHAIN:-INPUT} -j ACCEPT ${PROTO:+-p $PROTO} ${IN:+-i $IN} ${FROM:+-s $FROM} -m multiport --dports $1 -m conntrack --ctstate ESTABLISHED ${2:+-m comment --comment \"$2\"}"

sudo iptables -A ${INPUT_CHAIN:-INPUT} -j ACCEPT ${PROTO:+-p $PROTO} ${IN:+-i $IN} ${FROM:+-s $FROM} -m multiport --dports $1 -m conntrack --ctstate ESTABLISHED ${2:+-m comment --comment "$2"}

elif [[ "${MODE^^}" == 'SERVER' ]]; then

_debug_firewall "sudo iptables -A ${INPUT_CHAIN:-INPUT} -j ACCEPT ${PROTO:+-p $PROTO} ${IN:+-i $IN} ${FROM:+-s $FROM} -m multiport --dports $1 -m conntrack --ctstate NEW,ESTABLISHED ${2:+-m comment --comment \"$2\"}"

sudo iptables -A ${INPUT_CHAIN:-INPUT} -j ACCEPT ${PROTO:+-p $PROTO} ${IN:+-i $IN} ${FROM:+-s $FROM} -m multiport --dports $1 -m conntrack --ctstate NEW,ESTABLISHED ${2:+-m comment --comment "$2"}

else
_error "Undefined MODE"
return 1
fi

}

function _allow_out() {  # --in=<iface> --from=<cidr> --out=<iface> --to=<cidr> --proto=<tcp|udp> <port> [comment]

local i OUT TO PROTO

_debug_firewall "iptables.fun:_allow_out> MODE=$MODE _allow_out $@"

. longopts.sh  # <- $@ -> longopts[], longvals[], longargs[], $@

for i in ${!longopts[@]}; do
opt=${longopts[$i]}
val=${longvals[$i]}

case $opt in
from)
if [[ "$val" = 'any' ]]; then
FROM=0.0.0.0/0
else
FROM=$val
fi
;;
in) IN=$val;;
out) OUT=$val;;
to)
if [[ "$val" = 'any' ]]; then
TO=0.0.0.0/0
else
TO=$val
fi
;;
proto) PROTO=$val;;
esac

done

if [[ "${MODE^^}" == 'CLIENT' ]]; then

_debug_firewall "sudo iptables -A ${OUTPUT_CHAIN:-OUTPUT} -j ACCEPT ${PROTO:+-p $PROTO} ${OUT:+-o $OUT} ${TO:+-d $TO} -m multiport --sports $1 -m conntrack --ctstate NEW,ESTABLISHED ${2:+-m comment --comment \"$2\"}"

sudo iptables -A ${OUTPUT_CHAIN:-OUTPUT} -j ACCEPT ${PROTO:+-p $PROTO} ${OUT:+-o $OUT} ${TO:+-d $TO} -m multiport --sports $1 -m conntrack --ctstate NEW,ESTABLISHED ${2:+-m comment --comment "$2"}

elif [[ "${MODE^^}" == 'SERVER' ]]; then

_debug_firewall "sudo iptables -A ${OUTPUT_CHAIN:-OUTPUT} -j ACCEPT ${PROTO:+-p $PROTO} ${OUT:+-o $OUT} ${TO:+-d $TO} -m multiport --sports $1 -m conntrack --ctstate ESTABLISHED ${2:+-m comment --comment \"$2\"}"

sudo iptables -A ${OUTPUT_CHAIN:-OUTPUT} -j ACCEPT ${PROTO:+-p $PROTO} ${OUT:+-o $OUT} ${TO:+-d $TO} -m multiport --sports $1 -m conntrack --ctstate ESTABLISHED ${2:+-m comment --comment "$2"}

else
_error "Undefined MODE"
return 1
fi
}

function _allow() {

_allow_in $@ || return 1
_allow_out $@ || return 1

return 0
}



:<<\_x
_allow --in=$MNIC_IFACE --from=$MNIC_CIDR --to=any --proto=tcp 80 http

_allow_in --in="$MNIC_IFACE" --from=0.0.0.0/0 $ports "${comment}-server"

sudo ufw allow in on enp0s8 from 192.168.1.0/24 out on enp0s8 to 192.168.1.0/24 proto tcp port 80 comment http-server
sudo ufw allow from 192.168.1.0/24 in on enp0s8 to 192.168.1.0/24 out on enp0s8 proto tcp port 80 comment http-server
sudo ufw allow in on enp0s8 from 192.168.1.0/24 to any proto tcp port 80 comment http-server
_x


# ---------- ----------
function _deny() {

:
}


# ---------- ---------- ---------- ---------- ----------
INPUT_CHAIN=${INPUT_CHAIN:-PICASSO_INPUT}
OUTPUT_CHAIN=${OUTPUT_CHAIN:-PICASSO_OUTPUT}

:<<\_j
#alias _tcp_in_accept="sudo iptables -A ${INPUT_CHAIN:-INPUT} -p tcp -j ACCEPT "
function _tcp_in_accept() {
#[[ -v DEBUG && $DEBUG -ge 1 ]] && echo "${_TOP_:-$0}:$LINENO iptables -A ${INPUT_CHAIN:-INPUT} -p tcp -j ACCEPT $@ "
sudo iptables -A ${INPUT_CHAIN:-INPUT} -p tcp -j ACCEPT "$@"
}

#alias _tcp_out_accept="sudo iptables -A ${OUTPUT_CHAIN:-OUTPUT} -p tcp -j ACCEPT "
function _tcp_out_accept() {
#[[ -v DEBUG && $DEBUG -ge 1 ]] && echo "${_TOP_:-$0}:$LINENO iptables -A ${OUTPUT_CHAIN:-OUTPUT} -p tcp -j ACCEPT $@ "
sudo iptables -A ${OUTPUT_CHAIN:-OUTPUT} -p tcp -j ACCEPT "$@"
}

alias _udp_in_accept="sudo iptables -A ${INPUT_CHAIN:-INPUT} -p udp -j ACCEPT "
alias _udp_out_accept="sudo iptables -A ${OUTPUT_CHAIN:-OUTPUT} -p udp -j ACCEPT "
alias _tcp_in_drop="sudo iptables -A ${INPUT_CHAIN:-INPUT} -p tcp -j DROP "
alias _tcp_out_drop="sudo iptables -A ${OUTPUT_CHAIN:-OUTPUT} -p tcp -j DROP "
alias _udp_in_drop="sudo iptables -A ${INPUT_CHAIN:-INPUT} -p udp -j DROP "
alias _udp_out_drop="sudo iptables -A ${OUTPUT_CHAIN:-OUTPUT} -p udp -j DROP "
_j

# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
:<<\_c
_open_server_firewall <interface> <port/s> <comment> <type>
port/s - local destination port/s & local source port/s
type - UDP

workflow:
client(any port) -> server(specific port) -> client(any port)
_c
:<<\_c
interface...
127.0.0.1|localhost - do not open firewall
MNIC|$MNIC_IP - open firewall on $MNIC_IFACE ${MANAGEMENT_C}.0/$MANAGEMENT_PREFIX only
XNIC|$XNIC_IP - open firewall on "$XNIC_IFACE" $XNIC_CIDR only
NICSA - open firewall on array of interfaces
FNICSA - open firewall on array of interfaces
_c
:<<\_x
. iptables.fun
_open_server_firewall MNIC '53' "dns" UDP
-A PICASSO_INPUT -s 192.168.1.0/24 -i enp0s8 -p udp -m comment --comment "/mnt/r/picasso/provisioner.sh:67" -m multiport --dports 53 -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment -server -j ACCEPT
-A PICASSO_OUTPUT -d 192.168.1.0/24 -o enp0s8 -p udp -m comment --comment "/mnt/r/picasso/provisioner.sh:68" -m multiport --sports 53 -m conntrack --ctstate ESTABLISHED -m comment --comment -server -j ACCEPT
_x


function _open_server_firewall() {
_debug3 "_open_server_firewall $@"
#echo ghghhgso
#declare -p IFACES
#declare -p IFACE_MNIC

MODE=server

# synonymous...
case $1 in
MNIC_IP) local interfaces=MNIC;;
XNIC_IP) local interfaces=XNIC;;
*) local interfaces=$1;;
esac

local ports=${2/-/:}  # 1234-5678 -> 1234:5678
local comment=$3

if [[ -n "$4" ]]; then
tcp=false
echo $4 | grep -q -i 'tcp' && tcp=true
udp=false
echo $4 | grep -q -i 'udp' && udp=true
else
tcp=true  # default is tcp only
udp=false
fi
#_debug "wfow7fs90fwjf interfaces: $interfaces, ports: $ports, comment: $comment, udp: $udp, tcp: $tcp"

for ifacekey in ${interfaces//,/ }; do

_debug "sdf ifacekey: $ifacekey"

[[ -v IFACES[$ifacekey] ]] || {

_warn "sfs Missing IFACES[$ifacekey]"
continue
}

_debug2 "$(declare -p IFACES)"
_debug2 "IFACES[$ifacekey]: ${IFACES[$ifacekey]}"
_debug2 "$(declare -p IFACE_${ifacekey})"

declare -n _nic=${IFACES[$ifacekey]}  # _nic=IFACE_MNIC[]

_debug2 "$(declare -p _nic)"

[[ -v _nic[iface] ]] || {

_warn "llj Missing _nic[iface]"
continue
}

# iface overload
#local iface=${_nic[iface]}
local iface=${_nic[ifacekey],,}
local cidr=${_nic[cidr]}

_debug_firewall "$ifacekey - iface: $iface, cidr: $cidr"

case $ifacekey in

127.0.0.1|localhost)
:  # do not expose anything
;;

XNIC)

if $udp; then
#_udp_in_accept -i "$iface" -s 0.0.0.0/0 -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-server"
#_udp_out_accept -o "$iface" -d 0.0.0.0/0 -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-server"
_allow_in  --in=$iface --from=any --proto=udp $ports "${comment}-server"
_allow_out --out=$iface --to=any --proto=udp $ports "${comment}-server"
fi
if $tcp; then
#_tcp_in_accept -i "$iface" -s 0.0.0.0/0 -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-server"
#_tcp_out_accept -o "$iface" -d 0.0.0.0/0 -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-server"
_allow_in  --in=$iface --from=any --proto=tcp $ports "${comment}-server"
_allow_out --out=$iface --to=any --proto=tcp $ports "${comment}-server"
fi
;;

MNIC)

if $udp; then
#_udp_in_accept -i "$iface" ${cidr:+-s $cidr} -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-server"
#_udp_out_accept -o "$iface" ${cidr:+-d $cidr} -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-server"
_allow_in  --in=$iface ${cidr:+--from=$cidr} --proto=udp $ports "${comment}-server"
_allow_out --out=$iface --to=any --proto=udp $ports "${comment}-server"
fi
if $tcp; then
#_tcp_in_accept -i "$iface" ${cidr:+-s $cidr} -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-server"
#_tcp_out_accept -o "$iface" ${cidr:+-d $cidr} -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-server"
_allow_in  --in=$iface ${cidr:+--from=$cidr} --proto=tcp $ports "${comment}-server"
_allow_out --out=$iface --to=any --proto=tcp $ports "${comment}-server"
fi
;;

*)

if $udp; then
#_udp_in_accept -i "$iface" ${cidr:+-s $cidr} -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-server"
#_udp_out_accept -o "$iface" ${cidr:+-d $cidr} -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-server"
_allow_in  --in=$iface ${cidr:+--from=$cidr} --proto=udp $ports "${comment}-server"
_allow_out --out=$iface ${cidr:+--to=$cidr} --proto=udp $ports "${comment}-server"
fi
if $tcp; then
#_tcp_in_accept -i "$iface" ${cidr:+-s $cidr} -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-server"
#_tcp_out_accept -o "$iface" ${cidr:+-d $cidr} -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-server"
_allow_in  --in=$iface ${cidr:+--from=$cidr} --proto=tcp $ports "${comment}-server"
_allow_out --out=$iface ${cidr:+--to=$cidr} --proto=tcp $ports "${comment}-server"
fi
;;

junk)

echo "why am i here - fix this"

_debug3 "*)"

#if [[ -n "$MNIC_IP" && "$ifacekey" == "$MNIC_IP" ]]; then
if [[ "$ifacekey" == "$MNIC_IP" ]]; then
if $udp; then
_udp_in_accept -i "$MNIC_IFACE" -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-server"
_udp_out_accept -o "$MNIC_IFACE" -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-server"
fi
if $tcp; then
_tcp_in_accept -i "$MNIC_IFACE" -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-server"
_tcp_out_accept -o "$MNIC_IFACE" -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-server"
fi
#elif [[ -n "$XNIC_IP" && "$ifacekey" == "$XNIC_IP" ]]; then
elif [[ "$ifacekey" == "$XNIC_IP" ]]; then
if $udp; then
_udp_in_accept -i "$XNIC_IFACE" -s 0.0.0.0/0 -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-server"
_udp_out_accept -o "$XNIC_IFACE" -d 0.0.0.0/0 -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-server"
fi
if $tcp; then
_tcp_in_accept -i "$XNIC_IFACE" -s 0.0.0.0/0 -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-server"
_tcp_out_accept -o "$XNIC_IFACE" -d 0.0.0.0/0 -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-server"
fi
else
_debug3 "catch-all ifacekey: $ifacekey, ports: $ports, comment: $comment"
if $udp; then
# $ifacekey is an ip
_udp_in_accept -i $iface -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-server"
_udp_out_accept -o $iface -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-server"
fi
if $tcp; then
# $interface is an ip
_tcp_in_accept -i $iface -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-server"
_tcp_out_accept -o $iface -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-server"
fi
fi
;;
esac

:<<\_j
NICSA)

prev_IFACE=

for ifacekey in "${NICSA[@]}"; do

ifacekey="${ifacekey#"${ifacekey%%[![:space:]]*}"}"  # remove leading whitespace characters
ifacekey="${ifacekey%"${ifacekey##*[![:space:]]}"}"  # remove trailing whitespace characters

[[ -v IFACES[$ifacekey] ]] || {

_warn "Missing IFACES[$ifacekey]"
continue
}

declare -n _nic=${IFACES[$ifacekey]}

[[ -v _nic[iface] ]] || {

_warn "Missing _nic[iface]"
continue
}

local iface=${_nic[iface]}
local cidr=${_nic[cidr]}

[[ "$iface" == "${prev_IFACE}" ]] && continue

case $ifacekey in

XNIC)
if $udp; then
_udp_in_accept -i "$iface" -s 0.0.0.0/0 -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-server"
_udp_out_accept -o "$iface" -d 0.0.0.0/0 -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-server"
fi
if $tcp; then
_tcp_in_accept -i "$iface" -s 0.0.0.0/0 -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-server"
_tcp_out_accept -o "$iface" -d 0.0.0.0/0 -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-server"
fi
;;

*)
if $udp; then
_udp_in_accept -i "$iface" ${cidr:+-s $cidr} -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-server"
_udp_out_accept -o "$iface" ${cidr:+-d $cidr} -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-server"
fi
if $tcp; then
_tcp_in_accept -i "$iface" ${cidr:+-s $cidr} -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-server"
_tcp_out_accept -o "$iface" ${cidr:+-d $cidr} -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-server"
fi
;;

esac

prev_IFACE=$iface
done
;;

FNICSA)
#echo sfsfdsdf
#DEBUG=3
_debug3 "FNICSA[@]: ${FNICSA[@]}"

prev_IFACE=

for ifacekey in ${FNICSA[@]}; do  # ifacekey=MNIC|XNIC...

ifacekey="${ifacekey#"${ifacekey%%[![:space:]]*}"}"  # remove leading whitespace characters
ifacekey="${ifacekey%"${ifacekey##*[![:space:]]}"}"  # remove trailing whitespace characters

[[ -v IFACES[$ifacekey] ]] || {

_warn "Missing IFACES[$ifacekey]"
continue
}

declare -n _nic=${IFACES[$ifacekey]}

[[ -v _nic[iface] ]] || {

_warn "Missing _nic[iface]"
continue
}

local iface=${_nic[iface]}
local cidr=${_nic[cidr]}

[[ "$iface" == "$prev_IFACE" ]] && continue

_debug3
case $ifacekey in

XNIC)
if $udp; then
_udp_in_accept -i "$iface" -s 0.0.0.0/0 -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-server"
_udp_out_accept -o "$iface" -d 0.0.0.0/0 -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-server"
fi
if $tcp; then
_tcp_in_accept -i "$iface" -s 0.0.0.0/0 -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-server"
_tcp_out_accept -o "$iface" -d 0.0.0.0/0 -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-server"
fi
;;

*)
_debug_network "network: $network, prefix: $prefix"
_debug_network "$(env | grep MNIC)"
_debug_network "udp: $udp"
if $udp; then
_udp_in_accept -i "$iface" ${cidr:+-s $cidr} -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-server"
_udp_out_accept -o "$iface" ${cidr:+-d $cidr} -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-server"
fi
if $tcp; then
_debug_network "_tcp_in_accept -i \"$iface\" ${cidr:+-s $cidr} -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment \"${comment}-server\""
_tcp_in_accept -i "$iface" ${cidr:+-s $cidr} -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-server"
_tcp_out_accept -o "$iface" ${cidr:+-d $cidr} -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-server"
fi
;;

esac

prev_IFACE=$iface
done
:<<\_c
default: iptables -A PICASSO_INPUT -p tcp -j ACCEPT -i enp0s8 -s 192.168.1.0/24 -m multiport --dports 80,443 -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment -server
default: iptables -A PICASSO_OUTPUT -p tcp -j ACCEPT -o enp0s8 -d 192.168.1.0/24 -m multiport --sports 80,443 -m conntrack --ctstate ESTABLISHED -m comment --comment -server
default: iptables -A PICASSO_INPUT -p tcp -j ACCEPT -i enp0s9 -m multiport --dports 80,443 -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment -server
default: iptables -A PICASSO_OUTPUT -p tcp -j ACCEPT -o enp0s9 -m multiport --sports 80,443 -m conntrack --ctstate ESTABLISHED -m comment --comment -server
_c
;;
_j

done

return 0
}


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
:<<\__c
_open_client_firewall <interface> <remote destination port/s, remote source port/s> <comment>

_open_client_firewall MNIC 22 'ssh'
_open_client_firewall XNIC "80,443" 'www'

clients typically connect from random ports above 1024

local       remote
any port -> 80
any port -> 445

this assumes _iptables://input-drop-forward-drop-output-accept, because dports always refers to the remote port and does not impede any local port destinations
__c
:<<\__c
TODO:
for now i avoid the random port stuff by omitting the local source port

we could extend things like so:
_open_client_firewall FNICSA any:445 'Samba'
_open_client_firewall FNICSA any:80 'Samba'
__c


function _open_client_firewall() {
_debug3 "_open_client_firewall $@"

MODE=client

case $1 in
MNIC_IP) local interfaces=MNIC;;
XNIC_IP) local interfaces=XNIC;;
*) local interfaces=$1;;
esac

local ports=${2/-/:}  # 1234-5678 -> 1234:5678
local comment=$3

if [[ -n "$4" ]]; then
tcp=false
echo $4 | grep -q -i 'tcp' && tcp=true
udp=false
echo $4 | grep -q -i 'udp' && udp=true
else
tcp=true  # default is tcp
udp=false
fi
_debug3 "wfow7fs90fwjf2 interfaces: $interfaces, udp: $udp, tcp: $tcp, ports: $ports, comment: $comment"

local ifacekey

for ifacekey in ${interfaces//,/ }; do

#_debug "ssfd ifacekey: $ifacekey"

[[ -v IFACES[$ifacekey] ]] || {

_warn "Missing IFACES[$ifacekey]"
continue
}

declare -n _nic=${IFACES[$ifacekey]}

[[ -v _nic[iface] ]] || {

_warn "Missing _nic[iface]"
continue
}

# iface overload
#local iface=${_nic[iface]}
local iface=${_nic[ifacekey],,}
local cidr=${_nic[cidr]}

_debug_firewall "$ifacekey - iface: $iface, cidr: $cidr"

case $ifacekey in

XNIC)
if $tcp; then
#_tcp_out_accept -o "$iface" -d 0.0.0.0/0 -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-client"
#_tcp_in_accept -i "$iface" -s 0.0.0.0/0 -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-client"
_allow_out --out=$iface --to=any --proto=tcp $ports "${comment}-client"
_allow_in  --in=$iface --from=any --proto=tcp $ports "${comment}-client"
fi
if $udp; then
#_udp_out_accept -o "$iface" -d 0.0.0.0/0 -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-client"
#_udp_in_accept -i "$iface" -s 0.0.0.0/0 -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-client"
_allow_out --out=$iface --to=any --proto=udp $ports "${comment}-client"
_allow_in  --in=$iface --from=any --proto=udp $ports "${comment}-client"
fi
;;

MNIC)
if $tcp; then
#_tcp_out_accept -o "$iface" ${cidr:+-d $cidr} -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-client"
#_tcp_in_accept -i "$iface" ${cidr:+-s $cidr} -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-client"
_allow_out --out=$iface --to=any --proto=tcp $ports "${comment}-client"
_allow_in  --in=$iface ${cidr:+--from=$cidr} --proto=tcp $ports "${comment}-client"
fi
if $udp; then
#_udp_out_accept -o "$iface" ${cidr:+-d $cidr} -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-client"
#_udp_in_accept -i "$iface" ${cidr:+-s $cidr} -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-client"
_allow_out --out=$iface --to=any --proto=udp $ports "${comment}-client"
_allow_in  --in=$iface ${cidr:+--from=$cidr} --proto=udp $ports "${comment}-client"
fi
;;

*)
if $tcp; then
#_tcp_out_accept -o "$iface" ${cidr:+-d $cidr} -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-client"
#_tcp_in_accept -i "$iface" ${cidr:+-s $cidr} -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-client"
_allow_out --out=$iface ${cidr:+--to=$cidr} --proto=tcp $ports "${comment}-client"
_allow_in  --in=$iface ${cidr:+--from=$cidr} --proto=tcp $ports "${comment}-client"
fi
if $udp; then
#_udp_out_accept -o "$iface" ${cidr:+-d $cidr} -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-client"
#_udp_in_accept -i "$iface" ${cidr:+-s $cidr} -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-client"
_allow_out --out=$iface ${cidr:+--to=$cidr} --proto=udp $ports "${comment}-client"
_allow_in  --in=$iface ${cidr:+--from=$cidr} --proto=udp $ports "${comment}-client"
fi
;;

esac
:<<\_j

NICSA)

for ifacekey in "${NICSA[@]}"; do

ifacekey="${ifacekey#"${ifacekey%%[![:space:]]*}"}"  # remove leading whitespace characters
ifacekey="${ifacekey%"${ifacekey##*[![:space:]]}"}"  # remove trailing whitespace characters

_debug3 "s9s6flf0wf ifacekey: $ifacekey"

[[ -v IFACES[$ifacekey] ]] || {

_warn "Missing IFACES[$ifacekey]"

continue
}

declare -n _nic=${IFACES[$ifacekey]}

[[ -v _nic[iface] ]] || {

_warn "Missing _nic[iface]"

continue
}

local iface=${_nic[iface]}
local cidr=${_nic[cidr]}

_debug3 "s9s6flf0wf iface: $iface"

case $ifacekey in

XNIC)
if $tcp; then
#_tcp_out_accept -o "$iface" -d 0.0.0.0/0 -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-client"
#_tcp_in_accept -i "$iface" -s 0.0.0.0/0 -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-client"
_allow_out --out=$iface --to=any --proto=tcp $ports "${comment}-client"
_allow_in  --in=$iface --from=any --proto=tcp $ports "${comment}-client"
fi
if $udp; then
#_udp_out_accept -o "$iface" -d 0.0.0.0/0 -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-client"
#_udp_in_accept -i "$iface" -s 0.0.0.0/0 -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-client"
_allow_out --out=$iface --to=any --proto=udp $ports "${comment}-client"
_allow_in  --in=$iface --from=any --proto=udp $ports "${comment}-client"
fi
;;

*)
if $tcp; then
#_tcp_out_accept -o "$iface" ${cidr:+-d $cidr} -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-client"
#_tcp_in_accept -i "$iface" ${cidr:+-s $cidr} -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-client"
_allow_out --out=$iface ${cidr:+--to=$cidr} --proto=tcp $ports "${comment}-client"
_allow_in  --in=$iface ${cidr:+--from=$cidr} --proto=tcp $ports "${comment}-client"
fi
if $udp; then
_udp_out_accept -o "$iface" ${cidr:+-d $cidr} -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-client"
_udp_in_accept -i "$iface" ${cidr:+-s $cidr} -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-client"
fi
;;

esac

done
;;

FNICSA)

for ifacekey in ${FNICSA[@]}; do

ifacekey="${ifacekey#"${ifacekey%%[![:space:]]*}"}"  # remove leading whitespace characters
ifacekey="${ifacekey%"${ifacekey##*[![:space:]]}"}"  # remove trailing whitespace characters

[[ -v IFACES[$ifacekey] ]] || {

_warn "Missing IFACES[$ifacekey]"

continue
}

declare -n _nic=${IFACES[$ifacekey]}

[[ -v _nic[iface] ]] || {

_warn "Missing _nic[iface]"

continue
}

local iface=${_nic[iface]}
local cidr=${_nic[cidr]}

case $ifacekey in

XNIC)
if $tcp; then
_tcp_out_accept -o "$iface" -d 0.0.0.0/0 -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-client"
_tcp_in_accept -i "$iface" -s 0.0.0.0/0 -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-client"
fi
if $udp; then
_udp_out_accept -o "$iface" -d 0.0.0.0/0 -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-client"
_udp_in_accept -i "$iface" -s 0.0.0.0/0 -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-client"
fi
;;

*)
if $tcp; then
_tcp_out_accept -o "$iface" ${cidr:+-d $cidr} -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-client"
_tcp_in_accept -i "$iface" ${cidr:+-s $cidr} -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-client"
fi
if $udp; then
_udp_out_accept -o "$iface" ${cidr:+-d $cidr} -m multiport --dports $ports -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "${comment}-client"
_udp_in_accept -i "$iface" ${cidr:+-s $cidr} -m multiport --sports $ports -m conntrack --ctstate ESTABLISHED -m comment --comment "${comment}-client"
fi
;;

esac

done
;;
_j

done

return 0
}


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
:<<\_c
. iptables.fun && _iptables_accept_all
_c

function _iptables_accept_all() {

sudo iptables -F
sudo iptables -X
sudo iptables -t nat -F
sudo iptables -t nat -X
sudo iptables -t mangle -F
sudo iptables -t mangle -X
sudo iptables -P INPUT ACCEPT
sudo iptables -P FORWARD ACCEPT
sudo iptables -P OUTPUT ACCEPT

sudo iptables -N TEMP_IN  # necessary for extraneous script that may be expecting this namespace to exist
sudo iptables -N TEMP_OUT  # necessary for extraneous script that may be expecting this namespace to exist
}


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
:<<\_x
_open_server_firewall MNIC 80,443 test
_open_client_firewall MNIC 80,443 test
sudo iptables -L INPUT
sudo iptables -L OUTPUT
sudo iptables -L PICASSO_INPUT
sudo iptables -L PICASSO_OUTPUT

sudo journalctl -f | grep IPT
_x


# ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
true
