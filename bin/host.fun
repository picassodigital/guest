:<<\_c
. host.fun

_host_set_ip <name> <ip>
_host_set_ip test 1.2.3.4
_c

function _host_set_ip() {
local name=$1
local ip=$2  # MNIC_IP/XNIC_IP/

_debug3 "_host_set_ip $name $ip"

case $ip in
MNIC_IP|XNIC_IP) ip=${!ip} ;;
esac

#_debug3 "_host_set_ip name: $name, ip: $ip"

if grep -q "${name}$" /etc/hosts; then
# insert

sudo sed -r -i "s/^ *[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+( +${name})/${ip}\1/" /etc/hosts

else
# append

sudo bash -c "cat >> /etc/hosts" <<< "$ip $name"

fi

}


# ---------- ----------
:<<\_s
#getent hosts $1 | awk '{print $1}' | head -n 1  # returns remote matches like: ntp.phub.net.cable.rogers.com
#getent ahosts $1 | sed -n 's/ *STREAM.*//p'
_s

function _host_get_ip() {
awk "{for(i=1;i<=NF;i++){ if(\$i==\"$1\"){print \$1;exit;} } }" /etc/hosts  # first occurrence only
}

#awk "{for(i=1;i<=NF;i++){ if(\$i==\"$1\"){print \$1} } }" /etc/hosts  # all occurrences

:<<\_x
_host_get_ip controller
_x
:<<\_x
awk '$1  ~ /^controller/' /etc/hosts
awk '/controller/ { print $0 }' /etc/hosts
awk "/controller/ { print \$0 }" /etc/hosts
arg=controller
awk "/$arg/ { print \$0 }" /etc/hosts
awk "{ if ( match(\"^$arg$\", \$2 )) print \$1 }" /etc/hosts
_x
:<<\_x
awk '{for(i=1;i<=NF;i++){ if($i=="hostmq"){print $1} } }' /etc/hosts
awk '{for(i=1;i<=NF;i++){ if($i=="hostdb"){print $1} } }' /etc/hosts
_x


# ---------- ----------
function _inject_into_etc_hosts() {  # <OSNAME>
local OSNAME=$1
IP=IP_${OSNAME^^}
case ${!IP} in
MNIC_IP)
grep -q " ${OSNAME}$" /etc/hosts || sudo bash -c "cat >> /etc/hosts" <<< "$MNIC_IP ${OSNAME}"
;;
XNIC_IP)
grep -q " ${OSNAME}$" /etc/hosts || sudo bash -c "cat >> /etc/hosts" <<< "$XNIC_IP ${OSNAME}"
;;
*)
# remote...
grep -q " ${OSNAME}$" /etc/hosts || sudo bash -c "cat >> /etc/hosts" <<< "$IP ${OSNAME}"
;;
esac
}

:<<\_x
_inject_into_etc_hosts test
_x
