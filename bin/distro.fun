:<<\_c
. distro.fun $TARGET_DISTRO $TARGET_VERSION $TARGET_TYPE $TARGET_ARCH  # -> $DISTRO_MAJOR_VERSION, $DISTRO_MINOR_VERSION, $DISTRO_REVISION

. distro.fun  # <- /etc/os-release - use running version
. distro.fun ubuntu 18.04.5 - use explicit version

. distro.fun ubuntu 20.04.1 server amd64
set | grep DISTRO_
  DISTRO_NAME=ubuntu
  DISTRO_VERSION=20.04.1
  DISTRO_MAJOR_VERSION=20
  DISTRO_MINOR_VERSION=04
  DISTRO_REVISION=1
  DISTRO_TYPE=legacy-server
  DISTRO_ARCH=amd64

each OS may report their version differently
we need a generic means to deal with their version data

$DISTRO_VERSION is embedded in the basebox in 000-distro.env, and is consumeable by _distro_get_version

_distro_get_version $DISTRO_VERSION  # -> $DISTRO_MAJOR_VERSION, $DISTRO_MINOR_VERSION, $DISTRO_REVISION of the running distro

_distro_get_version 20.04.1  # -> $DISTRO_MAJOR_VERSION, $DISTRO_MINOR_VERSION, $DISTRO_REVISION of the specified distro
_c

#echo "sdfsdlf DEPOT_DISTRO_PATH: $DEPOT_DISTRO_PATH"

if [[ -n "$@" ]]; then

DISTRO_NAME=$1
DISTRO_VERSION=$2

else

. /etc/os-release  # -> $VERSION

DISTRO_NAME=$ID
DISTRO_VERSION=$(echo $VERSION | cut -d' ' -f1)  # 22.04.4
fi

#echo "sgsl DISTRO_NAME: $DISTRO_NAME, DISTRO_VERSION: $DISTRO_VERSION"

# ----------
case $DISTRO_NAME in

ubuntu)

DISTRO_TYPE=${3:-server}
DISTRO_ARCH=${4:-amd64}

#function _distro_get_version() {
if [[ "$DISTRO_VERSION" == *.* ]]; then
DISTRO_MAJOR_VERSION=$(echo $DISTRO_VERSION | /usr/bin/cut -d. -f1)  # 22
DISTRO_MINOR_VERSION=$(echo $DISTRO_VERSION | /usr/bin/cut -d. -f2)  # 04
DISTRO_REVISION=$(echo $DISTRO_VERSION | /usr/bin/cut -d. -f3)  # 2
DISTRO_MM_VERSION=${DISTRO_MAJOR_VERSION}${DISTRO_MINOR_VERSION}
else
DISTRO_MAJOR_VERSION=$DISTRO_VERSION  # 2204
unset DISTRO_MINOR_VERSION
unset DISTRO_REVISION
DISTRO_MM_VERSION=${DISTRO_MAJOR_VERSION}  # 2204
fi

# return stdout
#}

DEPOT_DISTRO_PATH=$METADATA_DIR/depot/com/ubuntu  # /opt/picasso/metadata_share/depot/com/ubuntu
#DEPOT_DISTRO_PATH=$METADATA_DIR/readonly/distro-images/${DISTRO_NAME}${DISTRO_MM_VERSION}  # /opt/picasso/metadata_share/depot/com/ubuntu

_debug_basebox "DEPOT_DISTRO_PATH: $DEPOT_DISTRO_PATH"

case $DISTRO_MAJOR_VERSION in
20)
case $DISTRO_TYPE in
server) DISTRO_TYPE=live-server;;
esac
#server) DISTRO_TYPE=legacy-server;;
;;
22)
case $DISTRO_TYPE in
server) DISTRO_TYPE=live-server;;
esac
;;
esac


# ---------- ----------
:<<\__c
#DISTRO_TYPE=server DISTRO_ARCH=amd64 
_distro_get_sha256_checksum server amd64  # <- $DISTRO_NAME, $DISTRO_MAJOR_VERSION, $DISTRO_MINOR_VERSION, $DISTRO_REVISION
_distro_get_sha256_checksum $DISTRO_TYPE $DISTRO_ARCH  # <- $DISTRO_NAME, $DISTRO_MAJOR_VERSION, $DISTRO_MINOR_VERSION, $DISTRO_REVISION
__c
function _distro_get_sha256_checksum() {
#local type=${1:-server}
#local arch=${2:-amd64}
:<<\_x
type=legacy-server
arch=amd64
_x

#_debug "_distro_get_sha256_checksum $@"

#[[ -v DISTRO_MAJOR_VERSION ]] || _distro_get_version $1
local release=${DISTRO_MAJOR_VERSION}.${DISTRO_MINOR_VERSION}  # release=20.04
[[ -n "$DISTRO_REVISION" ]] && release+=.${DISTRO_REVISION}  # release=20.04.1

#local DEPOT_DISTRO_PATH=$METADATA_DIR/depot  # /opt/picasso/metadata_share/depot

#echo "cat $DEPOT_DISTRO_PATH/$DISTRO_MM_VERSION/SHA256SUMS | grep ${DISTRO_NAME}-${release}-${DISTRO_TYPE}-${DISTRO_ARCH}.iso | /usr/bin/cut -d' ' -f1"

cat $DEPOT_DISTRO_PATH/$DISTRO_MM_VERSION/SHA256SUMS | grep ${DISTRO_NAME}-${release}-${DISTRO_TYPE}-${DISTRO_ARCH}.iso | /usr/bin/cut -d' ' -f1

# return stdout
}

;;

*) return 1;;

esac


# ---------- ----------
function _version() { echo "$@" | awk -F. '{ printf("%d%03d%03d%03d\n", $1,$2,$3,$4); }'; }


# ---------- ----------
:<<\_x
DISTRO_NAME=ubuntu
DISTRO_VERSION=18.04.5
DISTRO_TYPE=server
DISTRO_ARCH=amd64

DEPOT_DISTRO_PATH=$METADATA_DIR/depot/com/ubuntu

. distro.fun
_distro_get_version $DISTRO_VERSION  # -> $DISTRO_MAJOR_VERSION, $DISTRO_MINOR_VERSION, $DISTRO_REVISION
DISTRO_SHA256_CHECKSUM=$(_distro_get_sha256_checksum)

distro_filename=${DISTRO_NAME}-${DISTRO_VERSION}-${DISTRO_TYPE}-${DISTRO_ARCH}

LOCAL_ISO_PATHFILE="$METADATA_DIR/depot/com/ubuntu/$DISTRO_MM_VERSION/${distro_filename}.iso"
LOCAL_ISO_URL="file://$(convertpath -m $LOCAL_ISO_PATHFILE)"
_x


# ---------- ----------
true
