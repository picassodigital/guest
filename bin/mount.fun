:<<\_c
$PR/$PV/guest/bin/mount.fun
_c

. iptables.fun


# ---------- ----------
SMB_PASS=0

function _mount_smb() {

(( ++SMB_PASS == 1 )) && {  # once only

_is_installed cifs-utils || _install cifs-utils  # likely already installed in the basebox - _is_installed eliminates having to use sudo when the package is already installed, which eliminates the necessity of prompting for for a password


# By default windows' shares mount with the full permissions (777) in Linux. To change the default permission use the dir_mode and file_mode options to set directory and file permission.
# vers=2.1  # 2.1(Windows 7)/3.0(Windows 8+) - using 2.1 for both Win 7/10 support
# SMBv1 is not installed by default in Windows 10, version 1709 and Windows Server, version 1709
# iocharset=utf8 allows access to files with names in non-English languages
# uid/gid - If you need special permission (like chmod etc.), you�ll need to add a uid (short for �user id�) or gid (for �group id�) parameter to the share�s mount options
# sec=ntlmv2 - (ntlm: deprecated) (ntlmssp: default)
# noauto - whether to automount the share on boot

cat > ~/.smbcredentials <<!
username=${CIFS_USER:-$CIFS_USER_DEFAULT}
password=${CIFS_PASS:-$CIFS_PASS_DEFAULT}
!

chmod 400 ~/.smbcredentials
}


# ----------
_url_parse $DEST_ENDPOINT EP


# ----------
scheme=${EP[scheme]}  # <- 'posix-mnic-xnic'

[[ $scheme == *-* ]] || scheme+="-mnic"  # default

while true; do
[[ $scheme == *-* ]] || break

scheme=${scheme#*-}  # <- 'posix-mnic-xnic' -> 'mnic-xnic'
nic=${scheme%%-*}  # -> 'mnic'
[[ -z "$nic" ]] && break

nic=${nic^^}  # -> 'MNIC'

if [[ -v IFACES[$nic] ]]; then

_open_client_firewall $nic 445 'samba' 'TCP'

else
_error "_open_client_firewall $nic 445 'samba' 'TCP'"
return 1
fi
done

:<<\_x
scheme=posix-mnic-xnic

while true; do
[[ $scheme == *-* ]] || break
scheme=${scheme#*-}
nic=${scheme%%-*}
[[ -z "$nic" ]] && break
nic=${nic^^}
echo "inner nic: $nic"
done
_x


# ----------
DEST_MNT=${EP[path]}

[[ -d "$DEST_MNT" ]] || {

_info "Mounting $UNC <-> $DEST_MNT"

sudo bash -c "echo '$UNC $DEST_MNT cifs $OPTIONS 0 0' >> /etc/fstab"

sudo systemctl daemon-reload

_debug "021-mount> sudo mkdir -p $DEST_MNT"

sudo mkdir -p $DEST_MNT

_debug "021-mount> sudo mount $DEST_MNT"

sudo mount $DEST_MNT
}

_debug3 "021-mount> $(ls -l $DEST_MNT)"

_debug3 "021-mount> $(< /etc/fstab)"


# ----------
true
}


# ---------- ----------
NFS_PASS=0

function _mount_nfs() {

(( ++NFS_PASS == 1 )) && {  # once only

_is_installed nfs-common || _install nfs-common  # client

:<<\_x
sudo cat /etc/idmapd.conf
_x
}


# ----------
_url_parse $DEST_ENDPOINT EP  # -> EP[scheme]


# ----------
scheme=${EP[scheme]}  # <- 'posix-mnic-xnic'

[[ $scheme == *-* ]] || scheme+="-mnic"  # default

while true; do
[[ $scheme == *-* ]] || break

scheme=${scheme#*-}
nic=${scheme%%-*}
[[ -z "$nic" ]] && break

nic=${nic^^}

if [[ -v IFACES[$nic] ]]; then

_open_client_firewall $nic 111 'nfs-portmapper' 'TCP,UDP'
_open_client_firewall $nic 2049 'nfs-data' 'TCP,UDP'

else
_error "_open_client_firewall $nic 111 'nfs-portmapper' 'TCP,UDP'"
_error "_open_client_firewall $nic 2049 'nfs-data' 'TCP,UDP'"
return 1
fi
done


# ----------
DEST_MNT=${EP[path]}

[[ -d "$DEST_MNT" ]] || {

_info "Mounting $UNC <-> $DEST_MNT"

sudo bash -c "echo '$UNC $DEST_MNT nfs $OPTIONS 0 0' >> /etc/fstab"

sudo systemctl daemon-reload

_debug "021-mount> sudo mkdir -p $DEST_MNT"

sudo mkdir -p $DEST_MNT

_debug "021-mount> sudo mount $DEST_MNT"

sudo mount $DEST_MNT
}


# ----------
true
}


# ---------- ----------
:<<\_c
proof: $DESKSP/storage/block/iscsi/bootstrap initiator
_c

ISCSI_PASS=0

function _mount_iscsi() {

(( ++ISCSI_PASS == 1 )) && {  # once only

_is_installed open-iscsi || _install open-iscsi  # iscci initiator (client)

:<<\_x
which iscsid
which iscsiadm
_x
}


# ----------
_url_parse $DEST_ENDPOINT EP  # -> EP[scheme]


# ----------
scheme=${EP[scheme]}  # <- 'posix-mnic-xnic'

[[ $scheme == *-* ]] || scheme+="-mnic"  # default

while true; do
[[ $scheme == *-* ]] || break

scheme=${scheme#*-}
nic=${scheme%%-*}
[[ -z "$nic" ]] && break

nic=${nic^^}

if [[ -v IFACES[$nic] ]]; then

#_open_client_firewall $nic 860 'iscsi' 'TCP'  # necessary???
_open_client_firewall $nic 3260 'iscsi-discover' 'TCP'

else
#_error "_open_client_firewall $nic 860 'iscsi' 'TCP'"  # necessary???
_error "_open_client_firewall $nic 3260 'iscsi-discover' 'TCP'"
return 1
fi
done


# ----------
DEST_MNT=${EP[path]}

[[ -d "$DEST_MNT" ]] || {

_info "Mounting $UNC <-> $DEST_MNT"

sudo mkdir -p $DEST_MNT

_url_parse "iscsi:$UNC" SEP

_debug "sdvsvw9nm $(declare -p SEP)"

_debug "mount.fun:_mnt_iscsi> sudo iscsiadm -m discovery -t st -p ${SEP[hostname]}"

sudo iscsiadm -m discovery -t st -p ${SEP[hostname]}

INITIATOR_IQN=foobar

sudo bash -c "cat > /etc/iscsi/initiatorname.iscsi" <<!
InitiatorName=$INITIATOR_IQN
!
:<<\_x
iscsi-iname -g -p $INITIATOR_IQN > /etc/iscsi/initiatorname.iscsi
_x

sudo systemctl restart open-iscsi.service iscsid.service

}


# ----------
true
}


# ---------- ----------
true
