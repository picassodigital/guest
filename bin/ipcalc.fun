#!/usr/bin/env bash

:<<\_c
ipcalc.fun <cidr> [prefix]
```
IP=...
```

declare $(ipcalc.fun <cidr> [prefix])  # -> $IP...
_c

:<<\_x
ipcalc.fun 2.2.3.4/24
```
echo $IP
echo $CIDR
echo $NETWORK
echo $PREFIX
echo $NETMASK
echo $BROADCAST
```
_x

:<<\_x
XNIC_CIDR=10.0.0.1/24
```
XNIC_IP=10.0.0.1
XNIC_NETWORK=10.0.0.0/24
XNIC_NETWORK_C=10.0.0
XNIC_PREFIX=24
XNIC_NETMASK=255.255.255.0
XNIC_BROADCAST=10.0.0.255
XNIC_CIDR=10.0.0.0/24
```
_x

:<<\_x
test_CIDR=10.0.0.1/24
declare $(ipcalc.fun $test_CIDR test_)
echo $test_IP
echo $test_CIDR
echo $test_NETWORK
echo $test_PREFIX
echo $test_NETMASK
echo $test_BROADCAST
_x

:<<\_x
declare $(ipcalc.fun 10.0.0.1/24)
echo $IP
echo $CIDR
echo $NETWORK
echo $PREFIX
echo $NETMASK
echo $BROADCAST
_x

IPCALC_SCRIPT_DIR="$(dirname "${BASH_SOURCE[0]}")"


# ----------
:<<\_j
function _ipcalc() {

#_debug_network "_ipcalc @: $@"

local a n m b

local ipcalc_response="$($IPCALC_SCRIPT_DIR/ipcalc describe $1)"
:<<\_x
ipcalc_response="$(ipcalc describe 10.0.0.1/24)"
_x

#_debug_network "$(echo "$ipcalc_response")"

a=$(echo "$ipcalc_response" | grep "^Address:" | awk '{print $2}')
declare -g ${2}IP=${a#*=}

#_debug_network "declare -g ${2}IP=${a#*=}"

n=$(echo "$ipcalc_response" | grep "^Network:" | awk '{print $2}')
declare -g ${2}NETWORK=${n#*=}
declare -g ${2}NETWORK_C=${n%.*}
declare -g ${2}PREFIX=${n#*/}

#_debug_network "declare -g ${2}NETWORK=${n#*=}"
#_debug_network "declare -g ${2}NETWORK_C=${n%.*}"

m=$(echo "$ipcalc_response" | grep "^Netmask:" | awk '{print $2}')
declare -g ${2}NETMASK=${m#*=}

b=$(echo "$ipcalc_response" | grep "^Broadcast:" | awk '{print $2}')
declare -g ${2}BROADCAST=${b#*=}

declare -g ${2}CIDR=${n#*=}

true
}
_j

function _ipcalc() {

#_debug_network "_ipcalc @: $@"

local a n m b

local ipcalc_response="$($IPCALC_SCRIPT_DIR/ipcalc describe $1)"
:<<\_x
ipcalc_response="$(ipcalc describe 10.0.0.1/24)"
_x

#_debug_network "$(echo "$ipcalc_response")"

a=$(echo "$ipcalc_response" | grep "^Address:" | awk '{print $2}')
echo ${2}IP=${a#*=}

#_debug_network "declare -g ${2}IP=${a#*=}"

n=$(echo "$ipcalc_response" | grep "^Network:" | awk '{print $2}')
echo ${2}NETWORK=${n#*=}
echo ${2}NETWORK_C=${n%.*}
echo ${2}PREFIX=${n#*/}

#_debug_network "echo ${2}NETWORK=${n#*=}"
#_debug_network "echo ${2}NETWORK_C=${n%.*}"

m=$(echo "$ipcalc_response" | grep "^Netmask:" | awk '{print $2}')
echo ${2}NETMASK=${m#*=}

b=$(echo "$ipcalc_response" | grep "^Broadcast:" | awk '{print $2}')
echo ${2}BROADCAST=${b#*=}

echo ${2}CIDR=${n#*=}

true
}


# ---------- ----------
[[ -n "$@" ]] && _ipcalc $@
