:<<\_c
$PR/$PV/guest/bin/nftables-install.sh

https://wiki.debian.org/nftables#I_knew_the_iptables_syntax.__Is_there_a_new_syntax_in_nftables.
https://www.cyberciti.biz/faq/how-to-configure-firewall-with-ufw-on-ubuntu-20-04-lts/#google_vignette

FIREWALL=nftables://input-drop-forward-drop-output-drop

ufw rules are persistent - once UFW is enabled it runs across system reboots 
_c

FIREWALL=${FIREWALL:-nftables://input-drop-forward-drop-output-accept}


# ---------- ----------
function input-drop-forward-drop-output-drop() {

_build_default_chain $@

}


# ---------- ----------
function input-drop-forward-drop-output-accept() {

sudo ufw default allow outgoing

_build_default_chain $@

}


# ---------- ----------
function _build_default_chain() {  # <- [$IFACE $CIDR]

_debug_firewall "trusted_iface: $trusted_iface, trusted_cidr: $trusted_cidr"

if [[ -n "$trusted_iface" ]]; then

# SSH in from LAN(192.168.1.0/24) -> $trusted_iface(tcp/22)
#_tcp_input_accept --dport 22 -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "ssh"
#_tcp_output_accept --sport 22 -m conntrack --ctstate ESTABLISHED -m comment --comment "ssh"
#_tcp_input_accept -i $trusted_iface -s $trusted_cidr --dport 22 -m conntrack --ctstate NEW,ESTABLISHED -m comment --comment "ssh-server"
#_tcp_output_accept -o $trusted_iface -d $trusted_cidr --sport 22 -m conntrack --ctstate ESTABLISHED -m comment --comment "ssh-server"

sudo ufw allow in on $trusted_iface from $trusted_cidr proto tcp port 22 comment ssh-server
sudo ufw allow out on $trusted_iface to $trusted_cidr proto tcp port 22 comment ssh-server

:<<\_s
# SSH out to $trusted_iface/$trusted_cidr(tcp/22)
# Allow outgoing SSH connection: $trusted_iface(tcp/22) <-> LAN(192.168.1.0/24)
_tcp_output_accept -o $trusted_iface -d $trusted_cidr --dport 22 -m conntrack --ctstate NEW,ESTABLISHED
_tcp_input_accept -i $trusted_iface -s $trusted_cidr --sport 22 -m conntrack --ctstate ESTABLISHED
_s

# DHCP/BOOTP client
#_udp_output_accept -o $trusted_iface -m multiport --dports 67,68 -m comment --comment "dhcp-client"
#_udp_input_accept -i $trusted_iface -m multiport --sports 67,68 -m comment --comment "dhcp-client"
#_tcp_output_accept -o $trusted_iface -m multiport --dports 67,68 -m comment --comment "dhcp-client"
#_tcp_input_accept -i $trusted_iface -m multiport --sports 67,68 -m comment --comment "dhcp-client"
sudo ufw allow in on $trusted_iface from $trusted_cidr proto tcp port 67,68 comment dhcp-client  # proto tcp & udp
sudo ufw allow out on $trusted_iface to $trusted_cidr proto tcp port 67,68 comment dhcp-client  # proto tcp & udp
fi

}


# ---------- ----------
sudo DEBIAN_FRONTEND=noninteractive apt-get -qq install nfs-common ufw

[[ -v IFACES[MNIC] ]] && {

declare -n _nic=${IFACES[MNIC]}
trusted_iface=${_nic[iface]}
trusted_cidr=${_nic[cidr]}

_debug_firewall "trusted_iface: $trusted_iface, trusted_cidr: $trusted_cidr"
}

[[ -n $1 ]] && {

_debug_firewall "$1 $trusted_iface $trusted_cidr"

_url_parse $1 FW

${FW[hostname]} $trusted_iface $trusted_cidr || _return "${FW[hostname]} $trusted_iface $trusted_cidr"  # configure nftables
}


# ----------
sudo DEBIAN_FRONTEND=noninteractive apt-get -qq install expect

sudo /usr/bin/expect <<EOF
spawn sudo ufw enable
expect "Proceed with operation (y|n)?"
send "y\n"
expect eof
exit
EOF

:<<\_x
sudo status ufw.service

sudo ufw disable
_x
