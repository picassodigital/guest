[[ -v DEBUG && $DEBUG -ge 0 ]] && printf "\e[1;38;5;228m>>> %s @: %s\e[0m\n" "${BASH_SOURCE[0]}" "${*@Q}"

:<<\_c
$PR/$PV/guest/bin/kv.sh [dest] [KV_ENDPOINT]

dynamically inject the KV runtime into the guest

prov-endpoint.sh {

[[ -n "$KV_ENDPOINT" ]] && {
. kv.sh  # <- $KV_ENDPOINT
}

[[ -n "$L3_DOMAIN" ]] && {
#[[ -n "${KV[scheme]}" ]] && ln -s $PR/$PV/guest/fun/${KV[scheme]}.env $PR/fun/${KV[scheme]}.env
}
}
. guest-environment.sh {
}
_c

:<<\_x
. kv.sh  # <- $KV_ENDPOINT -> $PR/bin/{100-kv.env,101-kv.fun}, KV[]
_x

:<<\_c
'kv' is registered in dns as the Kv endpoint
redis${VDOM:+.$VDOM} would work as well; however, the generic 'kv' enables us to use any {KV[scheme]} at kv${VDOM:+.$VDOM}
_c

:<<\_c
is Kv single or multi domain?
Dns is multi-domain. Since Kv is deployed with Dns, that would make Kv multi-domain.
it makes a difference of where the kv.fun and kv.env files are written to (PR or NAMESP_CTX)
_c

:<<\_c
. $NAMESP_CTX/bin/010-kv.fun

we want to load this file from init.d, which means it will load whether there is a kv store or not

there are three conditions
KV[scheme] undefined - that's cool, we presume provisioning does not need it - return 0
KV[scheme] defined and not empty - load the appropriate handler - return 0
KV[scheme] defined and empty or not a valid handler - return 1
_c

#echo "sdsfdslfsf KV_ENDPOINT: $KV_ENDPOINT"
#DEBUG_KV=2

:<<\_c
our default environment file has been going here: $PR/bin/100-kv.env
if there is one KV for each FDOM, then it has to go here: ???
$NAMESP_CTX/bin/100-kv.env - this suffices for single KV instance per FDOM - which seems appropriate
each subsequent instance will overwrite $ENV_DIR/*
_c

PR_BIN=${1:-$PR/bin}

_debug_kv "PR_BIN: $PR_BIN"
#echo sfspfwpffsw8
#ls -l $PR

KV_ENDPOINT=${2:-$KV_ENDPOINT}

_debug_kv "KV_ENDPOINT: $KV_ENDPOINT"

_url_parse $KV_ENDPOINT KV || _return "_url_parse $KV_ENDPOINT KV"  # -> KV[]

_debug_kv "KV[scheme]: ${KV[scheme]}, KV[hostname]: ${KV[hostname]}, KV[port]: ${KV[port]}"

[[ -z "${KV[scheme]}" ]] && _return 'Unknown {KV[scheme]}'

[[ "${KV[hostname]}" =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]] || {  # explicit ip?

# no, not an ip - assume it is a hostname

KV[hostname]=$(_dns_lookup ${KV[hostname]})  # convert to ip
}

_debug_kv "KV[hostname]: ${KV[hostname]}"


# ----------
#echo sdslfssdlfslfdsj
#ls -l $PR
#ls -l $PR/bin

cat > $PR_BIN/100-kv.env <<!
export KV_ENDPOINT=$KV_ENDPOINT
_url_parse \$KV_ENDPOINT KV
!

_debug_kv "ln -sf $(which ${KV[scheme]}.fun) $PR_BIN/101-kv.fun"

ln -sf $(which ${KV[scheme]}.fun) $PR_BIN/101-kv.fun  # autoload

. $PR_BIN/100-kv.env
. $PR_BIN/101-kv.fun


# ----------
true
