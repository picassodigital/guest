:<<\_c
$PR/$PV/bin/redis.fun = $PR/$PV/host/bin/redis.fun

. redis.fun

*.fun on the host may be completely different than *.fun within the guest
HOST - $PR/$PV/host/bin/*.fun
GUEST - $PR/$PV/bin/*.fun
_c

#which redis-cli 1>/dev/null || _install redis-tools  # -> redis-cli

:<<\_s
alias redis-cli="redis-cli -h ${KV[hostname]} -p ${KV[port]}"
_s

:<<\_x
# redis-cli --raw isn't really RAW, it appends a newline

FQDN=www.picasso.digital

tar_file=$FQDN.tar
tar_file2=${FQDN}2.tar

# create tar file
tar -C $HOME -cvpf $tar_file picasso.cnf

tar -xvpf $tar_file --directory=$PWD
ls -l ~/picasso.cnf
ls -l $PWD/picasso.cnf
diff ~/picasso.cnf $PWD/picasso.cnf

_kv_set_file TARTEST $tar_file
_kv_get_file TARTEST $tar_file2
diff $tar_file $tar_file2

hexdump -x $tar_file
hexdump -x $tar_file2

redis-cli -h ${KV[hostname]} -p ${KV[port]} -x set TARTEST < $tar_file
hexdump -x <(redis-cli -h ${KV[hostname]} -p ${KV[port]} --raw get TARTEST)  # appends a newline
hexdump -x <(redis-cli -h ${KV[hostname]} -p ${KV[port]} --raw -d '' get TARTEST)  # -d never worked
hexdump -x <(redis-cli -h ${KV[hostname]} -p ${KV[port]} --raw get TARTEST | head -c -1)  # removes appended newline
_x


# ----------
:<<\_c
we are returning stdout; therefore, we cannot write anything else like _debug to it
redis-cli is designed to return error on unable to connect
and it ruturns 0 whether the key exists or not
_c

function _kv_get() {  # <- <key> $VDOM

. longopts.sh

for i in ${!longopts[@]}; do
opt=${longopts[$i]}
val=${longvals[$i]}
case $opt in
key) key=$val;;
esac
done


# ----------
#if $SEMA && [[ -n "$VDOM" ]]; then
if [[ -n "$VDOM" ]]; then
local key=${VDOM//./_}_${longargs[0]}  # namespace
else
local key=${longargs[0]}
fi


# ----------
_url_parse $KV_ENDPOINT KV || _return "_url_parse $KV_ENDPOINT KV"  # -> KV[]

local s=$(redis-cli -h ${KV[hostname]} -p ${KV[port]} get $key)

[[ -z "$s" ]] && return 1

[[ "$s" == '(nil)' ]] && return 1

redis-cli -h ${KV[hostname]} -p ${KV[port]} --raw get $key

# return $?
}
export -f _kv_get
:<<\_x
_kv_get DNS_KEY  # <- $VDOM
_x


# ----------
function _kv_set() {  # <- <key> <value> $VDOM

_debug_kv "VDOM=$VDOM, @: $@"

. longopts.sh

for i in ${!longopts[@]}; do
opt=${longopts[$i]}
val=${longvals[$i]}
case $opt in
key) key=$val;;
esac
done


# ----------
#if $SEMA && [[ -n "$VDOM" ]]; then
if [[ -n "$VDOM" ]]; then
local key=${VDOM//./_}_${longargs[0]}  # namespace
else
local key=${longargs[0]}
fi


# ----------
_url_parse $KV_ENDPOINT KV || _return "_url_parse $KV_ENDPOINT KV"  # -> KV[]

_debug_kv "_kv_set - rv=\$(redis-cli -h ${KV[hostname]} -p ${KV[port]} set $key ${longargs[1]})"

local rv=$(redis-cli -h ${KV[hostname]} -p ${KV[port]} set $key ${longargs[1]})

[[ $rv == 'OK' ]]  # return true/false
}
export -f _kv_set


# ----------
function _kv_delete() {

_debug_kv "VDOM=$VDOM, @: $@"

. longopts.sh

for i in ${!longopts[@]}; do
opt=${longopts[$i]}
val=${longvals[$i]}
case $opt in
key) key=$val;;
esac
done


# ----------
#if $SEMA && [[ -n "$VDOM" ]]; then
if [[ -n "$VDOM" ]]; then
local key=${VDOM//./_}_${longargs[0]}  # namespace
else
local key=${longargs[0]}
fi


# ----------
_url_parse $KV_ENDPOINT KV || _return "_url_parse $KV_ENDPOINT KV"  # -> KV[]

_debug_kv "_kv_delete - rv=\$(redis-cli -h ${KV[hostname]} -p ${KV[port]} del $key)"

local rv="$(redis-cli -h ${KV[hostname]} -p ${KV[port]} del $key)"

rv=${rv/(integer) /}

[[ $rv == 1 ]]
}
export -f _kv_delete


# ----------
:<<\_c
file -> key
_kv_set_file <key> <file>
_c

function _kv_set_file() {  # <- <key> <file> $VDOM

. longopts.sh

for i in ${!longopts[@]}; do
opt=${longopts[$i]}
val=${longvals[$i]}
case $opt in
key) key=$val;;
esac
done


# ----------
#if $SEMA && [[ -n "$VDOM" ]]; then
if [[ -n "$VDOM" ]]; then
local key=${VDOM//./_}_${longargs[0]}  # namespace
else
local key=${longargs[0]}
fi


# ----------
_url_parse $KV_ENDPOINT KV || _return "_url_parse $KV_ENDPOINT KV"  # -> KV[]

_debug_kv "_kv_set_file - rv=\$(redis-cli -h \${KV[hostname]} -p \${KV[port]} -x set $key < ${longargs[1]})"

local rv=$(redis-cli -h ${KV[hostname]} -p ${KV[port]} -x set $key < ${longargs[1]})

[[ $rv == 'OK' ]]  # return true/false
}
export -f _kv_set_file


# ----------
:<<\_c
key -> file
_kv_get_file <key>  # returns a $(mktemp -t picasso.XXXXXXXX)
_kv_get_file <key> [file]
_c

function _kv_get_file() {  # <- <key> <file> $VDOM

. longopts.sh

for i in ${!longopts[@]}; do
opt=${longopts[$i]}
val=${longvals[$i]}
case $opt in
key) key=$val;;
esac
done


# ----------
#if $SEMA && [[ -n "$VDOM" ]]; then
if [[ -n "$VDOM" ]]; then
local key=${VDOM//./_}_${longargs[0]}  # namespace
else
local key=${longargs[0]}
fi


# ----------
_url_parse $KV_ENDPOINT KV || _return "_url_parse $KV_ENDPOINT KV"  # -> KV[]

_debug_kv "_kv_get_file - KV[hostname]: ${KV[hostname]}, KV[port]: ${KV[port]}"

f=${longargs[1]:-$($(mktemp -t picasso.XXXXXXXX) || return 1)}

_debug_kv "_kv_get_file - key: $key, file: $f"

# redis-cli will tack on a newline to the end of the blob when you use the file redirection
# redis-cli apparently has a delimiter argument '-d <delimiter>', but it never seemed to function
# therefore, to remove the trailing newline we add '| head -c -1'

_debug "_kv_get_file - redis-cli -h ${KV[hostname]} -p ${KV[port]} --raw get $key | head -c -1 > $f"

redis-cli -h ${KV[hostname]} -p ${KV[port]} --raw get $key | head -c -1 > $f

#return $?
}
export -f _kv_get_file

true
