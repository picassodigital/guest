[[ -v DEBUG && $DEBUG -ge 0 ]] && printf "\e[1;38;5;228m>>> %s @: %s\e[0m\n" "${BASH_SOURCE[0]}" "${*@Q}"

:<<\_c
$PR/$PV/guest/bin/distro.sh  # -> stdout

this is a bootstrapping file that need not reside on the guest machines
except for the fact that it is used to seed baseboxes with their distro configuration

$DESKSP/picasso/basebox/$PBV/pre-provisioner.sh {
. distro.sh > $PR/bin/000-distro.env && . $PR/bin/000-distro.env  # -> $OS
}

in the future it could be moved here...
$PR/$PV/basebox/bin/distro.sh
and after provisioning, disposed of...
rm -r $PR/$PV/basebox
_c

:<<\_x
. $PR/$PV/guest/bin/distro.sh

. $PR/$PV/guest/bin/distro.sh > $PR/bin/000-distro.env && . $PR/bin/000-distro.env
_x


# ---------- ----------
echo anotherbigchange=true
#shopt -s expand_aliases  # aliases are not expanded when the shell is not interactive, unless the expand_aliases shell option is set using shopt

cat <<!

# ${BASH_SOURCE[0]}...

!


# ---------- ----------
arch=$(uname -m)
kernel=$(uname -r)

if [[ "$(expr substr $(echo $(uname -s) | tr -s '[:upper:]' '[:lower:]') 1 5)" == 'linux' ]]; then

# yes this is linux...

#if [[ "$kernel" =~ Microsoft ]]; then

if [[ -f "/etc/lsb-release" ]]; then

os=$(lsb_release -s -d)
export OS=ubuntu
#export OSV=$(lsb_release -sr)

:<<\_s
# setting this is global and peristent - the drawback is that manual installs are also affected which is unexpected by the user
echo 'debconf debconf/frontend select Noninteractive' | sudo debconf-set-selections
_s

elif [[ -f "/etc/debian_version" ]]; then

os="debian $(</etc/debian_version)"
export OS=debian

:<<\_s
echo 'debconf debconf/frontend select Noninteractive' | sudo debconf-set-selections
_s

elif [[ -f "/etc/fedora-release" ]]; then

export OS=fedora

. /etc/fedora-release
#export OSV=$VERSION_ID

export releasever=$(rpm -q --qf "%{version}\n" --whatprovides redhat-release)
export basearch=$(uname -m)

elif [[ -f "/etc/redhat-release" ]]; then

os=`cat /etc/redhat-release`
export OS=redhat

export releasever=$(rpm -q --qf "%{version}\n" --whatprovides redhat-release)
export basearch=$(uname -m)

elif [[ -f "/etc/arch-release" ]]; then

export OS=arch

else

export OS="$(echo $(uname -s) | tr -s '[:upper:]' '[:lower:]') $(uname -r)"

fi

export SYSTEMD_VERSION=$(systemctl --version 2> /dev/null | head -n1 | awk '{print $2}')

:<<\_s
elif [[ "$(echo $(uname -o) | tr -s '[:upper:]' '[:lower:]')" == "cygwin" ]]; then

#export OS=cygwin
export OS=Windows_NT

elif [[ "$(echo $(uname -o) | tr -s '[:upper:]' '[:lower:]')" == "msys" ]]; then

#export OS=msys
export OS=Windows_NT
_s

fi


# ---------- ----------
#get_distro

cat <<!
export OS=$OS
!


# ---------- ----------
:<<\_c
https://peteris.rocks/blog/quiet-and-unattended-installation-with-apt-get/
_c

case $OS in

arch)

cat <<\!

function _is_installed() { echo TODO; }

function _install() {
if [[ -v DEBUG ]]; then

if (( DEBUG == 0 )); then
sudo 1>/dev/null pacman --noconfirm -S "$@"
elif (( DEBUG < -1 )); then
sudo &>/dev/null pacman --noconfirm -S "$@"
elif (( DEBUG < -0 )); then
sudo 1>/dev/null 2>&1 pacman --noconfirm -S "$@"
else
pacman --noconfirm -S "$@"
fi

else
sudo 1>/dev/null pacman --noconfirm -S "$@"
fi
}
export -f _install
!
;;


fedora)

cat <<\!

function _is_installed() { dnf -q list installed $1; }

function _install() {
if [[ -v DEBUG ]]; then

if [[ $DEBUG == 0 ]]; then
sudo dnf -qy install "$@"
elif [[ $DEBUG < -1 ]]; then
sudo &>/dev/null dnf -qy install "$@"
elif [[ $DEBUG < 0 ]]; then
sudo 1>/dev/null 2>&1 dnf -qy install "$@"
else
sudo dnf -y install "$@"
fi

else
sudo 1>/dev/null sudo dnf -qy install "$@"
fi
}
export -f _install
!
;;

debian|ubuntu)

DISTRO_VERSION=$(lsb_release -d -s | cut -d' ' -f2)

cat <<\!

function _is_installed() {
[[ "$(2>/dev/null /usr/bin/dpkg-query -W -f '${db:Status-Abbrev}' $1 | tr -d '[:space:]')" == 'ii' ]]
}

function _install() {
if [[ -v DEBUG ]]; then

if (( DEBUG == 0 )); then
sudo 1>/dev/null DEBIAN_FRONTEND=noninteractive apt-get -q -y -o=Dpkg::Use-Pty=0 install "$@"
elif (( DEBUG < -1 )); then
sudo &>/dev/null DEBIAN_FRONTEND=noninteractive apt-get -qq -o=Dpkg::Use-Pty=0 install "$@"
elif (( DEBUG < -0 )); then
sudo 1>/dev/null 2>&1 DEBIAN_FRONTEND=noninteractive apt-get -qq -o=Dpkg::Use-Pty=0 install "$@"
else
sudo DEBIAN_FRONTEND=noninteractive apt-get -y -o=Dpkg::Use-Pty=0 install "$@"
fi

else
sudo 1>/dev/null DEBIAN_FRONTEND=noninteractive apt-get -qq -o=Dpkg::Use-Pty=0 install "$@"
fi
}
export -f _install
!

cat <<!

export DISTRO_VERSION=$DISTRO_VERSION
export DISTRO_MAJOR_VERSION=$(echo $DISTRO_VERSION | /usr/bin/cut -d. -f1)
export DISTRO_MINOR_VERSION=$(echo $DISTRO_VERSION | /usr/bin/cut -d. -f2)
export DISTRO_REVISION=$(echo $DISTRO_VERSION | /usr/bin/cut -d. -f3)
!

[[ -n "$SYSTEMD_VERSION" ]] && {

cat <<!
export SYSTEMD_VERSION=$SYSTEMD_VERSION
!
}

;;

esac


:<<\_c
version() - convert dotted vertion to whole number so it may be compared to other whole-number versions
_c
:<<\_s
cat <<\!

function version() { echo "$@" | awk -F. '{ printf("%d%03d%03d%03d\n", $1,$2,$3,$4); }'; }
export -f version
!
_s

true
